export const environment = {
  production: true,
  servicesUrl: 'http://localhost/apiVentasCr/public/api/', // local
  // servicesUrl: 'https://servicio.electrotaller265.com/api/', // server test
  uploadUrl: 'http://localhost/apiVentasCr/public/'
};
