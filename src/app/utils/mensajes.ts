import { Injectable } from '@angular/core';
@Injectable({ providedIn: 'root' })

export class Mensajes {
  // mensajes tabla
  itemsPorPagina = 'Items por página';
  itemSiguiente = 'Página siguiente';
  itemAnterior = 'Página anterior';

}