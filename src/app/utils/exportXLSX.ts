import * as XLSX from 'xlsx';

export class ExportXLSX {

    static downloadXLSX(encabezado: string[], lista: [][], nombreArchivo: string) {
        const listaImprimir: any[] = [];
        listaImprimir.push(encabezado);
        lista.forEach(element => {
            listaImprimir.push(element);
        })

        // Generar hoja de excel
        const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(listaImprimir);

        // Generar libro de excel
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, nombreArchivo);

        // Guardar a archivo
        XLSX.writeFile(wb, `${nombreArchivo}.xlsx`);
    }

    /**
     * 
     * @param encabezado1 Lista de encabezados para hoja 1
     * @param encabezado2 Lista de encabezados para hoja 2
     * @param lista1 Lista de datos para hoja 1
     * @param lista2 Lista de datos para hoja 2
     * @param nombreH1 Nombre para datos de hoja 1 y nombre de archivo
     * @param nombreH2 nombre para datos de hoja 2 y nombre de archivo
     */
    static downloadXLSX2WS(encabezado1: string[], encabezado2: string[], lista1: [][], lista2: [][], nombreH1: string, nombreH2: string) {
        const listaImprimir1: any[] = [];
        listaImprimir1.push(encabezado1);
        lista1.forEach(element => {
            listaImprimir1.push(element);
        });

        const listaImprimir2: any[] = [];
        listaImprimir2.push(encabezado2);
        lista2.forEach(element => {
            listaImprimir2.push(element);
        });

        // Generar hoja de excel
        const ws1: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(listaImprimir1);
        const ws2: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(listaImprimir2);

        // Generar libro de excel
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws1, nombreH1);
        XLSX.utils.book_append_sheet(wb, ws2, nombreH2);

        // Guardar a archivo
        XLSX.writeFile(wb, `${nombreH1}.xlsx`);
    }
}
