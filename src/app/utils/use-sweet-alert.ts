import Swal, { SweetAlertIcon } from 'sweetalert2';

export class SweetAlert {

    /**
     *
     * @param allowOutsideClick boolean
     * @param type success|error|warning|info|question
     * @param text contenido del elemento
     * @param title titulo del elemento?opcional
     * @param loading boolean muestra icono de carga
     * @param showConfirmButton boolean
     * @param timer tiempo que se muestra
     */

    static show(allowOutsideClick: boolean, icon: SweetAlertIcon, text: string, title?: string, loading?: boolean,
        showConfirmButton = true, timer? : number ) {
        Swal.fire({
          allowOutsideClick: allowOutsideClick,
          icon: icon,
          title: title,
          text: text,
          showConfirmButton: showConfirmButton,
          timer: timer,
        });
        if (loading) {
          Swal.showLoading();
        }
      }

    /**
     * Cerrar Sweet Alert
     */
    static close() {
        Swal.close();
    }

    /**
     * 
     * @param title Titulo del elemento
     * @param text Información de consecuencias de la acción
     * @param type error|warning
     * @param confirmButtonText Texto del botón de confirmación OK|Aceptar
     * @param cancelButtonText Texto del botón de cancelación  NO|Cancelar
     */
    static confirm(title: string, text: string, icon: SweetAlertIcon, confirmButtonText: string, cancelButtonText: string){
    return Swal.fire({      
      title: title,
      text: text,
      icon: icon,
      showCancelButton: true,
      confirmButtonText: confirmButtonText,
      cancelButtonText: cancelButtonText,
      showLoaderOnConfirm: true
    });
    }


    static html(allowOutsideClick: boolean, icon: SweetAlertIcon, html: string, title?: string, loading?: boolean, showConfirmButton = true){
      return Swal.fire({
        allowOutsideClick: allowOutsideClick,
        title: title,
        icon: icon,
        html:html,
        showCloseButton: showConfirmButton,
        showCancelButton: false,
      })
    }

}