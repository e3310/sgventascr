import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

export class PdfMake {

    protected static createTableHeaders(encabezado: string[]) {
        let headers = [];
        encabezado.forEach(element => {
            let celda = {
                text: element, style: 'tableHeader'
            }
            headers.push(celda);
        })
        return headers;
    }

    protected static createTable(encabezado: string[], lista: [][]) {
        let headers = this.createTableHeaders(encabezado);
        let table = [];
        table.push(headers);
        for ( var index = 0; index < lista.length; index++ ) {
            let row = lista[index];
            table.push( row );
        }
        return table;
    }

    /**
     * Crea pdf para ser descargado o visualizado
     * @param encabezado 
     * @param lista 
     * @param titulo 
     * @param autor 
     * @param orientacion     portrait|landscape
     */
    static createPdf( encabezado: string[], lista: [][], titulo: string, autor: string, orientacion?: string) {

        pdfMake.vfs = pdfFonts.pdfMake.vfs;

        let value = this.createTable(encabezado, lista);

        var dd = {
            pageOrientation: orientacion,
            info: {
                title: titulo,
                author: autor
            },
            content: [
                { text: '', fontSize: 14, bold: true, margin: [0, 0, 0, 8] },
                '',
                {
                    style: 'tableExample',
                    table: {
                        headerRows: 1,
                        body: value,
                        border: [true, true, true, true],
                        fillColor: '#eeffee'
                    },
                    layout: 'noBorders'
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    margin: [0, 5, 0, 15]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            }

        }
        const pdfDocGenerator = pdfMake.createPdf( dd );
        return pdfDocGenerator;
    }
    static downloadPdf(pdfDocGenerator: any, nombreArchivo: string) {
      pdfDocGenerator.download(nombreArchivo + '.pdf');
    }

    static viewPdf(pdfDocGenerator: any, targetElement: any){
        pdfDocGenerator.getDataUrl((dataUrl) => {
            // const targetElement = document.querySelector('#iframeContainer');
            const iframe = document.createElement('iframe');
            iframe.src = dataUrl;
            iframe.style.height = '100vh';
            iframe.style.width = '100%';
            targetElement.appendChild(iframe);
        });
    }

}