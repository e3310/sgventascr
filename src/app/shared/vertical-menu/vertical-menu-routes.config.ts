import { ConstantsService } from 'app/services/constants.service';
import { RouteInfo } from './vertical-menu.metadata';

//Sidebar menu Routes and data
export let ROUTES: RouteInfo[];
if( ConstantsService.getNegocioKey() && ConstantsService.getNegocioValue() ){
ROUTES=  [
  {
    path: '', title: 'Gestión de Negocios', icon: 'fas fa-briefcase', class: 'has-sub', isExternalLink: false,
    submenu: [
        { path: '/core/negocio/lista', title: 'Lista de negocios', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    ]
  },
  {
    path: '', title: 'Gestión de Clientes', icon: 'fas fa-user-alt', class: 'has-sub', isExternalLink: false,
    submenu: [
        { path: '/core/pais/lista', title: 'Lista de paises', icon: 'fas fa-globe-americas', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/cliente/lista', title: 'Lista de clientes', icon: 'fas fa-user-alt', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    ]
  },
  {
    path: '', title: 'Gestión de Productos', icon: 'fas fa-box-open', class: 'has-sub', isExternalLink: false,
    submenu: [
        { path: '/core/producto/lista', title: 'Lista de productos', icon: 'fas fa-boxes', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/compra/lista', title: 'Lista de compras', icon: 'fas fa-dolly', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    ]
  },
  {
    path: '', title: 'Gestión de Ventas', icon: 'fas fa-star', class: 'has-sub', isExternalLink: false,
    submenu: [
        { path: '/core/tipoFPago/lista', title: 'Lista de Tipos de Formas de Pago', icon: 'fas fa-money-check-alt', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/formaPago/lista', title: 'Lista de Cuentas', icon: 'fas fa-donate', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/vendedor/lista', title: 'Lista de Vendedores', icon: 'fas fa-user-tie', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/venta/lista', title: 'Lista de Ventas', icon: 'fas fa-shopping-cart', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        { path: '/core/kardex/lista', title: 'Lista de Movimientos', icon: 'fas fa-pallet', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
      ]
  },
  {
    path: '', title: 'Gestión de Usuarios', icon: 'fas fa-user-lock', class: 'has-sub', isExternalLink: false,
    submenu: [
        { path: '/core/usuario/lista', title: 'Lista de Usuarios', icon: 'fas fa-users-cog', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
        // { path: '/core/cuentaBancaria/lista', title: 'Lista de Cuentas Bancarias', icon: 'fas fa-donate', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
    ]
  }
  // {
  //   path: '/page', title: 'Page', icon: 'ft-home', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
  // },
  // {
  //   path: '', title: 'Menu Levels', icon: 'ft-align-left', class: 'has-sub', badge: '3', badgeClass: 'badge badge-pill badge-danger float-right mr-1 mt-1', isExternalLink: false,
  //   submenu: [
  //       { path: '/YOUR-ROUTE-PATH', title: 'Second Level', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //       {
  //           path: '', title: 'Second Level Child', icon: 'ft-arrow-right submenu-icon', class: 'has-sub', badge: '', badgeClass: '', isExternalLink: false,
  //           submenu: [
  //               { path: '/YOUR-ROUTE-PATH', title: 'Third Level 1.1', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //               { path: '/YOUR-ROUTE-PATH', title: 'Third Level 1.2', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
  //           ]
  //       },
  //   ]
  // },
];
}else{
  ROUTES = [
    {
      path: '', title: 'Gestión de Negocios', icon: 'fas fa-briefcase', class: 'has-sub', isExternalLink: false,
      submenu: [
          { path: '/core/negocio/lista', title: 'Lista de negocios', icon: 'ft-arrow-right submenu-icon', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: [] },
      ]
    }
  ];
}
