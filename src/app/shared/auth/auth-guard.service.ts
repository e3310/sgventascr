// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
// import { Injectable } from '@angular/core';
// import { AuthService } from './auth.service';

// @Injectable()
// export class AuthGuard implements CanActivate {

//   constructor(private authService: AuthService, private router: Router) { }

//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//     let isAuth = this.authService.isAuthenticated();
//     if (!isAuth) {
//       this.router.navigate(['/pages/login']);
//     }
//     else {
//       return true;
//     }
//   }
// }

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private login: LoginService, private router: Router){

  }

  canActivate(): boolean {
    // console.log('AuthGuard corriendo');
    if (this.login.estaAutenticado()) {
      return true;
    } else {
      this.router.navigate(['/pages/login']);
      return false;
    }

  }
}