export class TipoFormaPago {
    id?: number;
    nombre: string;
    descripcion?: string;
    neg_id: number;
    estado?: string;

}