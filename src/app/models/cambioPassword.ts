export class CambioPassword {
    userId: string;
    userName: string;
    passOld: string;
    passNew: string;
    passNewConfirm: string;
}