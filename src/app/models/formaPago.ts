export class FormaPago {
    id?:number ;
    numero:string ;
    tipo_cuenta:string ;
    saldo?:number =0;
    entidad_bancaria:string ;
    nombres_titular:string ;            
    apellidos_titular:string ;
    ci_titular:string ;
    telefono_titular?:string ;
    email_titular?: string;
    estado:string ;
    tipofp: number;
    neg_id:number ;
    usuario_registro?:number ;
    fecha_registro?: Date ;

}