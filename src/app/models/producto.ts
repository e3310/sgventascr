export class Producto {
    id?: number;
    nombre?: string;
    costo: number;
    precio? : number;
    stock: number;
    neg_id: number;
}