export class Venta {
    id? : number;
    cli_id: number;
    vend_id: number;
    tfp_id: number;
    fp_id: number;
    fecha_registro: Date;
    kar_id?: number = 0;
    // total: total;
    estado: string;
    neg_id: number;
    usuario_registro: number;
    //detalle
    paq_id ?: number;
    cantidad? : number;
    total_det? : number;
    bonificacion? : number;
    //detalles adicionales
    nombrePaquete? : string;
    nombreVendedor? : string;
    userCliente? : string;
    cantidadVenta?: number;
    detalles : VentaDetalle[] = [];
}

export class VentaDetalle{
    paq_id ?: number;
    cantidad : number;
    total_det : number;
    bonificacion : number;
}