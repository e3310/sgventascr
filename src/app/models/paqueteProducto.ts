export class PaqueteProducto {
    id?: number;
    descripcion: string;
    prod_id: number;
    nro_unidades : number;
    precio_venta: number;
    neg_id: number;
}