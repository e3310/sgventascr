export class RepCliVenta {
    id?: number;
    tipo: string
    mes: string; //2 caracteres
    anio: string; // 4 caracteres
    neg_id: number;
    fecha_inicio?: Date;
    fecha_fin?: Date;
    fecha_generado?: Date;
    //DETALLE
    rcvd_id?: number;    
    cli_id: any; // para carga masiva 
    nro_creditos: number;
    bonificacion?:number = 0;    
}