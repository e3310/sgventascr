export class Usuario {
    idPersona : number;
    cedula: string;
    nombres: string;
    apellidos: string;
    genero?: string;
    telefono?: string;
    email?: string;
    direccion?: string;
    ciudad: string;
    pais?:string;
    tipo_nombre?:string;
    idUsuario?:number;
    nameUsuario?:string;
    estado?:string;
    neg_id?: number;
    idTipo?: number;
}