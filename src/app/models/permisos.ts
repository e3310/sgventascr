export class Permiso {
    isAuthorized: AccionPage= { abr:'A', state: false};
    isAuthCreate: AccionPage= { abr:'C', state: false};
    isAuthRead: AccionPage= { abr:'R', state: false};
    isAuthUpdate: AccionPage= { abr:'U', state: false};
    isAuthDesactivate: AccionPage= { abr:'D', state: false};
    isAuthSearch: AccionPage= { abr:'S', state: false};
    isAuthMass: AccionPage= { abr:'M', state: false};
    isAuthChangeType: AccionPage= { abr:'T', state: false};
    isAuthOptSelect: AccionPage= { abr:'O', state: false};
    isAuthLogout: AccionPage= { abr:'L', state: false};
    isAuthParam: AccionPage= { abr:'P', state: false};
}

export class AccionPage {
    abr: string;
    state: boolean;
}