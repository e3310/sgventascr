export class Compra {
    id?:number;
    pro_id?:number;
    cantidad:number;
    costo:number;
    total:number;
    estado?: string;            
    neg_id:number;
    fecha_registro?: Date;
    usuario_registro?:number;
}