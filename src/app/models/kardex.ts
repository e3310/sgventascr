export class Kardex {
    id?: number;
    fechaRegistro: Date;
    movimiento: string;
    neg_id: number;
    estado: string;
    cantidad: number;
    total_detalle: number;
    stock: number;
    costo: number;
}
