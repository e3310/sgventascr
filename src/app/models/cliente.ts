export class Cliente {
    id?: number;
    usuario_credito: string;
    nombres?: string;
    apellidos?: string;
    telefono : string;
    pais_id : any; // para enviar cadena en carga masiva
    ci?: string;
    ciudad?: string;
    direccion?: string;
    email?: string;
    genero?: string;
    estado?: string;
    neg_id: number;
    
    pais_nombre?: string;
    usuario_registro?: number;
    fechaRegistro?: Date;
    //
}