import { Injectable } from '@angular/core';
import { SweetAlert } from '../../utils/use-sweet-alert';

@Injectable({
  providedIn: 'root'
})
export class ErrorRequestService {
  public isDialogOpen: Boolean = false;
  constructor() { }
  openDialog(data): any {
      SweetAlert.show( false, 'error',`Contacte con personal de sistemas y especifique la siguiente informacion:
      Text: ${data['statusText']}.
      StatusCode: ${data['status']}.`
       , 'Error en la estructura de la solicitud' );
      // Swal.fire({
      //     title: 'Error al procesar su solicitud',
      //     html: `<span class="badge badge-pill badge-secondary">${data['statusText']} ${data['status']}</span>
      //     <p>${data['reason']}</p>`,
      //     type: 'error',
      // });
  }
}
