import { Component, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from 'app/shared/auth/auth.service';
import { NgxSpinnerService } from "ngx-spinner";
import { LoginService } from '../../../services/login.service';
import { Login } from '../../../models/login';
import { HttpErrorResponse } from '@angular/common/http';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { ConstantsService } from 'app/services/constants.service';
import { MatDialog } from '@angular/material/dialog';
import { ChangePasswordComponent } from '../../../core/usuario/change-password/change-password.component';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

  loginFormSubmitted = false;
  isLoginFailed = false;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
    // rememberMe: new FormControl(true)
  });




  constructor(private router: Router, private authService: AuthService, private loginService:LoginService ,
    private spinner: NgxSpinnerService, private matDialog: MatDialog,
    private route: ActivatedRoute) {
  }

  get lf() {
    return this.loginForm.controls;
  }

  // On submit button click
  onSubmit() {   
    this.actionActivar();
    this.loginFormSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    let loginObject : Login ={
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
      device: navigator.userAgent
    }    
    this.spinner.show(undefined,
      {
        type: 'ball-triangle-path',
        size: 'medium',
        bdColor: 'rgba(0, 0, 0, 0.8)',
        color: '#fff',
        fullScreen: true
      });

    this.loginService.login(loginObject).subscribe( result => {
      console.log(result);
      if(result['status']=== 200){
        this.loginService.guardarToken(result);       
        this.spinner.hide();
        this.router.navigate(['/core/welcome']);
      }else{
        this.isLoginFailed = true;
        this.spinner.hide();
        console.log('error', result)
      }
    }, (error: HttpErrorResponse) => {      
      this.spinner.hide();
      console.log(error);
      if(error['status']===403){
        SweetAlert.show(false, 'warning', 'Su usuario no se encuentra activo.', 'Imposible acceder al sistema', false, true);
      }else if (error['status']===401){
        this.actionActivar();
      }
    });

      // let islogin = this.loginService.login(loginObject);
      // console.log(islogin);
      // if(islogin["codigo"] === true){
      //   console.log('login ok');
        
      //   this.spinner.hide();
      //   this.router.navigate(['/core/welcome']);
      // }else{
      //   this.isLoginFailed = true;
      //   this.spinner.hide();
      //   console.log('error')
      // }

    // this.authService.signinUser(this.loginForm.value.username, this.loginForm.value.password)
    //   .then((res) => {
    //     this.spinner.hide();
    //     this.router.navigate(['/core/welcome']);
    //   })
    //   .catch((err) => {
    //     this.isLoginFailed = true;
    //     this.spinner.hide();
    //     console.log('error: ' + err)
    //   }
    //   );
  }

  crearElementoModal(userKey: number, userName: string){
    return {
      accion: 'activar',
      userKey,
      userName
    }
  }

  actionActivar(){
    let datos = this.crearElementoModal(Number(ConstantsService.getUserKey()), ConstantsService.getUserName());
    
    console.log(datos);
    
    const dialogRef = this.matDialog.open(ChangePasswordComponent, {
      width: '30rem',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      if(data === 'SAVE'){
        this.router.navigate(['/core/welcome']);
      }      
    });
  }

}
