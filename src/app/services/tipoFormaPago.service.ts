import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { TipoFormaPago } from '../models/tipoFormaPago';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceTipoFormaPago {

  servicesUrl = environment.servicesUrl;
  serviceName = 'tiposFPago';

    constructor(private httpClient: HttpClient) { }

    getTipoFPago(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    saveTipoFPago(tipoFPago: TipoFormaPago) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, tipoFPago, httpOtions);
    }

    updateTipoFPago(formaPagoNew: TipoFormaPago, idTipoFPago: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idTipoFPago}`, formaPagoNew );
    }

    cambiarEstadoTipoFPago(idFPago: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idFPago}`, null );
    }

    getTipoFPagoActivos(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos/${idNegocio}`)
      .pipe(map(data => data['data']));
    }
}
