// import { Injectable } from '@angular/core';
// // import { environment } from '../../environments/environment';

// @Injectable({
//   providedIn: 'root'
// })
import * as CryptoJS from 'crypto-js';
import { Permiso } from '../models/permisos';

export class ConstantsService {
//   readonly baseAppUrl: string = environment.baseAppUrl;
//   readonly codigoApp: string = 'SINC';
    public static readonly pageSizeOptions = [5, 10, 20, 50, 100];
    public static readonly pageSize = 5;
    public static readonly listaEstados = ['ACTIVO','INACTIVO']
    public static readonly listaMovimientosKardex = [{id:'C', value: 'COMPRA'},{id:'V', value:'VENTA'},{id:'I', value:'CARGA INICIAL'}];
    public static readonly listaEstadosUsuarios = [{id:'A', value:'ACTIVO'},{id:'I', value:'INACTIVO'}, {id:'P', value:'PENDIENTE'}]
    public static readonly listaTiposCuenta = [{id:'A', value:'AHORROS'},{id:'C', value:'CORRIENTE'}]
    public static stringCuentaBancaria = 'CUENTA BANCARIA';
//   readonly pageSizeDefault = 0;
    private static _idNegocio: number = null;
    private static _nombreNegocio: string = null;
    private static SECRET_KEY = 'Ber1g0';
    //TIPOS DE USUARIO
    public static readonly typeUserAdmin = 'A';
    public static readonly typeUserVendor = 'V';

    
  // 

  //   // let data = CryptoJS.AES.encrypt('Hola@', SECRET_KEY);

  //   // console.log('ENC '+data.toString());
  //   // let dec = CryptoJS.AES.decrypt(data, SECRET_KEY);

  //   // console.log('DEC' + dec.toString(CryptoJS.enc.Utf8));

  public static listaGeneros = [{value: 'M', nombre:'Masculino'},
{value: 'F', nombre: 'Femenino'},
{value: 'O', nombre: 'Otro'}]

public static formatos = [ { value: 'pdf', icon: 'fas fa-file-pdf text-danger', name: 'PDF'},
{ value: 'excel', icon: 'fas fa-file-excel text-success', name: 'Microsoft Excel'}];
// { value: 'word', name: 'Microsoft Word'},
// { value: 'rtf', name: 'Formato RTF'},
// { value: 'csv', name: 'Valores separados por caracteres (CSV)'},
// { value: 'xml', name: 'XML' }];


  constructor() { }

    static get idNegocio(): number {
        return this._idNegocio;
    }
    
    static set idNegocio(value: number) {
        this._idNegocio = value;
    }

    static get nombreNegocio(): string {
    return this._nombreNegocio;
    }
    
    static set nombreNegocio(value: string) {
        this._nombreNegocio = value;
    }

   
    public static saveSessionNegocio() {
        if(this.idNegocio != null && this.nombreNegocio != null ){
            let negocioKey = CryptoJS.AES.encrypt(this.idNegocio.toString(), this.SECRET_KEY);
            sessionStorage.setItem('negocioKey', negocioKey.toString());
            sessionStorage.setItem('negocioValue', this.nombreNegocio.toString());
        }
    }

    public static getNegocioKey() :string {
        if (sessionStorage.getItem('negocioKey')) {
            let negocioKeyDecrypt = CryptoJS.AES.decrypt(sessionStorage.getItem('negocioKey'), this.SECRET_KEY);
            return negocioKeyDecrypt.toString(CryptoJS.enc.Utf8);
        }else return null;
    }

    public static getNegocioValue() :string {
        if (sessionStorage.getItem('negocioValue')) {
            return sessionStorage.getItem('negocioValue');
        }else return null;
    }

    public static getUserKey() :string {
        if (sessionStorage.getItem('userKey')) {
            let userKeyDecrypt = CryptoJS.AES.decrypt(sessionStorage.getItem('userKey'), this.SECRET_KEY);
            return userKeyDecrypt.toString(CryptoJS.enc.Utf8);
        }else return null;
    }

    public static getUserName() :string {
        if (sessionStorage.getItem('userName')) {
            return sessionStorage.getItem('userName');
        }else return null;
    }

    public static getUserType() :string {
        if (sessionStorage.getItem('type')) {
            let userKeyDecrypt = CryptoJS.AES.decrypt(sessionStorage.getItem('type'), this.SECRET_KEY);
            return userKeyDecrypt.toString(CryptoJS.enc.Utf8);
        }else return null;
    }

    private static getGrantSession(abr:string) {
        if (sessionStorage.getItem('grants')) {
            let grants = JSON.parse(sessionStorage.getItem('grants'));
            let pages = Object.keys(grants);
            let permiso = pages.find(x=>x === abr );            
            if(permiso){
                return grants[abr];
            }
        }else return null;
    }

    public static verifyPermisos(permiso: Permiso, abrPage: string, actionsPage: string){  
        let permisosPagina = 'CRUD'//this.getGrantSession(abrPage);
        if(permisosPagina){
            permiso.isAuthorized.state = true;
            for (let i = 0; i < permisosPagina.length; i++) {
            const accion = actionsPage.includes(permisosPagina[i]) ? permisosPagina[i] :'';
            let arr = Object.keys(permiso).filter(el=> permiso[el]['abr'] === accion);
                if(arr.length>0){
                permiso[arr[0]]['state']= true;     
                }               
            }
            console.log(permiso);    
        }
    }

    public static encrypt(value: string){
        let valueEncrypt = CryptoJS.AES.encrypt(value, this.SECRET_KEY);
        return valueEncrypt.toString();
    }
    public static decrypt( value: string) {
        let valueDecrypt = CryptoJS.AES.decrypt(value, this.SECRET_KEY);
        return valueDecrypt.toString(CryptoJS.enc.Utf8);
    }
}