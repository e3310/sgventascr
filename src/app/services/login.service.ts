import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../models/login';
import { ConstantsService } from './constants.service';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  user: string;
  password : string;
  token: string;
  servicesUrl = environment.servicesUrl;
  // serverUrl = environment.serverUrl;
  // user = 'admin';
  // password ='@dm1n2o2l';
  esAutenticado= false;
  userName = 'Usuario Pruebas';

  constructor(private httpClient: HttpClient) {
      this.leerToken();
   }

  public login(login: Login) {
    return this.httpClient.post( `${this.servicesUrl}login`, login);
  }

  public logout(keyUser: number){
    let body = {
      userkey: keyUser
    };
    return this.httpClient.post( `${this.servicesUrl}logout`, body);
  }

  clearSession() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userKey');
    sessionStorage.removeItem('userName');
    sessionStorage.removeItem('grants');
    sessionStorage.removeItem('type');      
    sessionStorage.removeItem('dateValid');
    sessionStorage.removeItem('negocioKey');
    sessionStorage.removeItem('negocioValue');
  }

  fechaValida(fecha: string){
    let dateTime = fecha.split(' ');
    let dateArray = dateTime[0].split('/');
    let date = `${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`
    let time = `${dateTime[1]}`;
    let valida = new Date(`${date} ${time}`)
    return valida;
    // let transfecha1= fecha.replace(new RegExp('/', 'g'), '-');
    // return transfecha1;
  }

  guardarToken(result: any) {
      this.user = ConstantsService.encrypt(result["userInfo"]["userKey"].toString());
      this.userName = result["userInfo"]["userName"];
      this.token = result["token"];
      let hoy = new Date(result["tokenCreation"].toString());
      hoy.setSeconds(3600);
      sessionStorage.setItem('token', this.token);
      sessionStorage.setItem('userKey', this.user);
      sessionStorage.setItem('userName', this.userName);
      sessionStorage.setItem('grants', JSON.stringify(result["grants"]));
      sessionStorage.setItem('type', ConstantsService.encrypt(result["userInfo"]["typeName"].substr(0,1)));      
      sessionStorage.setItem('dateValid', hoy.getTime().toString());
  }

  leerToken() {
      if (sessionStorage.getItem('userKey') && sessionStorage.getItem('token')) {
        this.user = sessionStorage.getItem('userKey');
        this.token = sessionStorage.getItem('token');
      } else {
        this.user = '';
        this.token = '';
      }
  }

  estaAutenticado(): boolean {  
      if (!this.token) {
        return false;
      }
      const expira = Number(sessionStorage.getItem('dateValid'));
      const tokenFromSession = sessionStorage.getItem('token');
      const user = sessionStorage.getItem('userKey');
      const expiraDate = new Date().getTime();
      // expiraDate.setTime(expira);

      console.log('EXPIRA',expiraDate);
      console.log('EXPIRASesion',expira);

      if (expiraDate <= expira && tokenFromSession === this.token && user === this.user ) {
        return true;
      }

    }

  // estaAutenticado(): boolean {   
  //   if (sessionStorage.getItem('userKey') && this.user === ConstantsService.decrypt(sessionStorage.getItem('userKey'))) {
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }

}
