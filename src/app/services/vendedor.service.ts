import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceVendedor {

  servicesUrl = environment.servicesUrl;
  serviceName = 'vendors';

    constructor(private httpClient: HttpClient) { }

    cambiarEstadoVendedor(idUsuario: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idUsuario}`, null );
    }

    getVendedoresActivos(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    getVendedoresPorUser(idUsuario: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/show/${idUsuario}`);
    }

}
