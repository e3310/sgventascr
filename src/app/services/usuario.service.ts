import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Usuario } from '../models/usuario';
import { CambioPassword } from '../models/cambioPassword';

@Injectable({
  providedIn: 'root'
})
export class ServiceUsuario {

  servicesUrl = environment.servicesUrl;
  serviceName = 'users';

    constructor(private httpClient: HttpClient) { }

    getUsersAdmin(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/allAdmin`)
      .pipe(map(data => data['data']));
    }

    getUsersVendedor(idNegocio: number){      
      console.log(`${this.servicesUrl}${this.serviceName}/allVendedor/${idNegocio}`);
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/allVendedor/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    saveUserAdmin(usuario: Usuario) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/crearAdmin`, usuario );
    }

    saveUserVendor(usuario: Usuario) {
      return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/crearVendedor`, usuario );
  }

    // updatePais(paisNew: Pais, idPais: number) {
    //   return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idPais}`, paisNew );
    // }

    cambiarEstadoUsuario(idUsuario: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idUsuario}`, null );
    }

    cambiarPassword(body: CambioPassword) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarPassword`, body );
    }

    cambiarTipo(body:any,idUsuario: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarTipo/${idUsuario}`, body );
    }

    activarPendiente(body: CambioPassword) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/activarPendiente`, body );
    }
}
