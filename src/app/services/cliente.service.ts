import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Cliente } from '../models/cliente';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json',
  'Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceCliente {

  servicesUrl = environment.servicesUrl;
  serviceName = 'clientes';

    constructor(private httpClient: HttpClient) { }

    getClientes(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    saveCliente(cliente: Cliente) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, cliente, httpOtions);
    }

    updateCliente(clienteNew: Cliente, idCliente: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idCliente}`, clienteNew );
    }

    cambiarEstadoCliente(idCliente: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idCliente}`, null );
    }

    searchClientes(idNegocio: number, fieldSearch: string, valueSearch: string) {
      let body ={
        idNegocio : idNegocio,
        value: valueSearch,
        field: fieldSearch
      };
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/search`, body )
        .pipe(map(data => data['data']));
    }

    saveClienteMasivo(datos: any) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/masivo`, datos, httpOtions );
    }

    getClientesActivos(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    // (file: File): Observable<APIResponse> {
    //   const url = `${environment.apiURL}/api/other/upload`;
    
    //   const formData = new FormData();
    //   formData.append("file", file, file.name);
    
    //   return this.http.post<APIResponse>(url, { formData });
    // }
}
