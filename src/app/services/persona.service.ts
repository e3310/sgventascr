import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class ServicePersona {
  servicesUrl = environment.servicesUrl;
  serviceName = 'person';

  constructor(private httpClient: HttpClient) { }

  updatePersona(personaNew: Usuario) {
    return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${personaNew.idPersona}`, personaNew );
  }
}
