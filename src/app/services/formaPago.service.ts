import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FormaPago } from '../models/formaPago';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceFormaPago {

  servicesUrl = environment.servicesUrl;
  serviceName = 'formasPago';

    constructor(private httpClient: HttpClient) { }

    getFormasPago(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    saveFormaPago(formaPago: FormaPago) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, formaPago, httpOtions);
    }

    updateFormaPago(cuentaBancariaNew: FormaPago, idFormaPago: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idFormaPago}`, cuentaBancariaNew );
    }

    cambiarEstadoFormaPago(idFormaPago: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idFormaPago}`, null );
    }

    getFormasPagoActivos(idNegocio: number, idTipo: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos/${idNegocio}/${idTipo}`)
      .pipe(map(data => data['data']));
    }
}
