import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceTipoUsuario {

  servicesUrl = environment.servicesUrl;
  serviceName = 'typeUser';

    constructor(private httpClient: HttpClient) { }

    getTipoAdmin(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/admin`)
      .pipe(map(data => data['data']));
    }

    getTipoVendor(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/vendor`)
      .pipe(map(data => data['data']));
    }
}
