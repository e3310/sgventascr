import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Venta } from '../models/venta';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceVenta {

  servicesUrl = environment.servicesUrl;
  serviceName = 'sales';

  constructor(private httpClient: HttpClient) { }

  getVentas(idNegocio: number){      
    return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
    .pipe(map(data => data['data']));
  }

  saveVenta(venta: Venta) {
      return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, venta, httpOtions);
  }

  cambiarEstadoVenta(idVenta: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idVenta}`, null );
  }

  searchVentas(idNegocio: number, valor1: string, valor2: string, campo: string) {
    let body ={
      idNegocio : idNegocio,
      value1: valor1,
      value2 : valor2,
      field : campo
    };
    console.log(body);    
      return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/search`, body )
      .pipe(map(data => data['data']));
  }
}
