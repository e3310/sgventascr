import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Pais } from '../models/pais';

@Injectable({
  providedIn: 'root'
})
export class ServicePais {
  
  servicesUrl = environment.servicesUrl;
  serviceName = 'paises';

    constructor(private httpClient: HttpClient) { }

    getPaises(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}`)
      .pipe(map(data => data['data']));
    }

    getPaisesActivos(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos`)
      .pipe(map(data => data['data']));
    }

    savePais(pais: Pais) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, pais );
    }

    updatePais(paisNew: Pais, idPais: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idPais}`, paisNew );
    }

    cambiarEstadoPais(idPais: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idPais}`, null );
    }
}
