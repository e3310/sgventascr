import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment.prod';
import { Negocio } from '../models/negocio';


const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceNegocio {

  servicesUrl = environment.servicesUrl;
  serviceName = 'negocios';

    constructor(private httpClient: HttpClient) { }

  //   getNegocios():Promise<Negocio[]> {      
  //     return this.httpClient.get<Negocio[]>( `${this.servicesUrl}${this.serviceName}`)
  //     .pipe(map(data => data['data'])).toPromise();
  // }
    getNegocios(){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}`)
      .pipe(map(data => data['data']));
    }

    saveNegocio(negocio: Negocio) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, negocio );
    }

    updateNegocio(negocioNew: Negocio, idNegocio: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idNegocio}`, negocioNew );
    }

    cambiarEstadoNegocio(idNegocio: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idNegocio}`, null );
    }

    // getChofer(codChofer: number) {
    //     return this.httpClient.get( `${this.servicesUrl}Chofer/${codChofer}`);
    // }






}
