import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceKardex {

  servicesUrl = environment.servicesUrl;
  serviceName = 'kardex';

  constructor(private httpClient: HttpClient) { }

  getKardex(idNegocio: number){      
    return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
    .pipe(map(data => data['data']));
  }

  recalculoKardex(idNegocio: number, fechaInicio: string) {
    let body ={
      idNegocio : idNegocio,
      startDate: fechaInicio
    };
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}`, body );
  }

  searchKardex(idNegocio: number, valor1: string, valor2: string, campo: string) {
    let body ={
      idNegocio : idNegocio,
      value1: valor1,
      value2 : valor2,
      field : campo
    };
    console.log(body);    
      return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/search`, body )
      .pipe(map(data => data['data']));
  }

  showMovimiento(idNegocio: number, idKardex: number, movimiento: string) {
    let body ={
      idNegocio : idNegocio,
      idKardex: idKardex,
      movimiento: movimiento
    };
      return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/show`, body )
      .pipe(map(data => data['data']));
  }
}
