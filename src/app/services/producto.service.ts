import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Producto } from '../models/producto';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServiceProducto {

  servicesUrl = environment.servicesUrl;
  serviceName = 'productos';

    constructor(private httpClient: HttpClient) { }

    getProductos(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    getProductosCount(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/count/${idNegocio}`);
      // .pipe(map(data => data['data']));
    }

    saveProducto(producto: Producto) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, producto, httpOtions);
    }
}
