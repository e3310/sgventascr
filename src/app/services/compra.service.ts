import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Compra } from '../models/compra';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})

export class ServiceCompra {

  servicesUrl = environment.servicesUrl;
  serviceName = 'purchases';

    constructor(private httpClient: HttpClient) { }

    getCompras(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    saveCompra(compra: Compra) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, compra, httpOtions);
    }

    // updateCompra(cuentaBancariaNew: Compra, idCompra: number) {
    //   return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idCompra}`, cuentaBancariaNew );
    // }

    cambiarEstadoCompra(idCompra: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idCompra}`, null );
    }

    searchCompras(idNegocio: number, fInicio: string, fFin: string) {
      let body ={
        idNegocio : idNegocio,
        fechaInicio: fInicio,
        fechaFin: fFin
      };
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}/search`, body )
        .pipe(map(data => data['data']));
    }
  
}
