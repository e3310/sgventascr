import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { PaqueteProducto } from '../models/paqueteProducto';

const httpOtions = {
  headers: new HttpHeaders( { 'Content-Type': 'application/json','Access-Control-Allow-Origin':'*' } )
}

@Injectable({
  providedIn: 'root'
})
export class ServicePaqueteProducto {

  servicesUrl = environment.servicesUrl;
  serviceName = 'paquetes';

    constructor(private httpClient: HttpClient) { }

    getPaquetes(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/all/${idNegocio}`)
      .pipe(map(data => data['data']));
    }

    savePaquete(paquete: PaqueteProducto) {
        return this.httpClient.post( `${this.servicesUrl}${this.serviceName}`, paquete, httpOtions);
    }

    updatePaquete(paqueteNew: PaqueteProducto, idPaquete: number) {
      return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/${idPaquete}`, paqueteNew );
    }

    cambiarEstadoPaquete(idPaquete: number) {
        return this.httpClient.put( `${this.servicesUrl}${this.serviceName}/cambiarEstado/${idPaquete}`, null );
    }

    getPaquetesActivos(idNegocio: number){      
      return this.httpClient.get( `${this.servicesUrl}${this.serviceName}/activos/${idNegocio}`)
      .pipe(map(data => data['data']));
    }
}
