import { Component, OnInit, Inject } from '@angular/core';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Pais } from '../../../models/pais';
import { ServicePais } from '../../../services/pais.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-pais',
  templateUrl: './create-pais.component.html',
  styleUrls: ['./create-pais.component.scss']
})
export class CreatePaisComponent implements OnInit {
  
  _ABREVIATURA= 'PAISC'
  accion:string;
  title: string;
  paisSelected: Pais = new Pais();

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;

  constructor(private fb: FormBuilder, private _servicePais: ServicePais, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.accion=parametros.accion;
      this.paisSelected = parametros.pais;
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    this.initValidators();
    this.initialize();
  }

  initialize() {
    if (this.accion === 'c') {
      this.title = 'Crear nuevo pais';
      this.form.get('nombre').setValue('');      
    } else {
      this.title ='Editar Pais';
      this.form.get('nombre').setValue(this.paisSelected.nombre);
    }
    SweetAlert.close();
  }

  initValidators() {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.maxLength(100)]]
     });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarPaisCrear() {
    let pais : Pais = {
      nombre : this.form.get('nombre').value
    }
    return pais;
  }

  asignarChoferActualizar() {
  }

  serviceCreatepais(){
    this._servicePais.savePais(this.asignarPaisCrear()).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar pais');
    });
  }

  serviceUpdatepais(){
    this._servicePais.updatePais(this.asignarPaisCrear(), this.paisSelected.id).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar pais');
    });
  }

  guardar(){
    this.submitted = true;
    if (this.form.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        console.log('creacion');
        this.serviceCreatepais();
        // this.verificarCreacionChofer();
      } else {
        console.log('update');
        this.serviceUpdatepais();
      }
    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.salir();
  }
}
