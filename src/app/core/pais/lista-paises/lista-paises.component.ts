import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ColumnMode,
  DatatableComponent,
  SelectionType
} from '@swimlane/ngx-datatable';
import {MatDialog} from '@angular/material/dialog';
import { Pais } from '../../../models/pais';
import { ServicePais } from '../../../services/pais.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { CreatePaisComponent } from '../create-pais/create-pais.component';
import { MatTableDataSource } from '@angular/material/table';
import { ConstantsService } from '../../../services/constants.service';
import { MatPaginator } from '@angular/material/paginator';
import { Mensajes } from '../../../utils/mensajes';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-paises',
  templateUrl: './lista-paises.component.html',
  styleUrls: ['./lista-paises.component.scss']
})
export class ListaPaisesComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'PAISL';
  _ACCIONES = 'CRUD';  
  _PERMISO = new Permiso();

  listaPaises: Pais [] = [];
  listaEstados = ['ACTIVO','INACTIVO']
  isLoading = true;
  paisSeleccionado: Pais = new Pais();

  //Información MatTable
  dataSource: MatTableDataSource<Pais> = new MatTableDataSource<Pais>(this.listaPaises);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;

  @ViewChild('paginator', { static: false }) set paginator(pais: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && pais !== undefined ){
      this.dataSource.paginator = pais;
      pais._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      pais._intl.previousPageLabel = this.mensajes.itemAnterior;
      pais._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }

  displayedColumns: string[] = [
    'acciones',
    'nombre',
    'estado'
  ];
  
  constructor(private _servicePais: ServicePais, private matDialog: MatDialog, private mensajes: Mensajes) {
    ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
    // this.getPaises();
   }

   ngOnInit():void{
     if(this._PERMISO.isAuthorized.state === true){
      this.getPaises();
     }    
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }


   getPaises() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de paiss', true, false);
    }
    this._servicePais.getPaises().subscribe( (result: Pais[]) => {
      this.listaPaises = result;
      this.asignarMatTable(this.listaPaises);
      // this.ngxDatatable.element.click();
      console.log(this.listaPaises)
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(paises: Pais[]){
    this.dataSource.data = paises;
    // this.dataSource.paginator = this.paginator;
    // this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string,pais?: Pais){
    return {
      accion,
      pais: pais ? pais: null
    }
  }


  actionCreate(accion: string,pais?: Pais){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, pais)
    }
    const dialogRef = this.matDialog.open(CreatePaisComponent, {
      // width: '90%',
      // height: '90%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getPaises();
    });
  }

  cambiarEstado(pais: Pais){
    let mensaje =`Se cambiará el estado del pais ${pais.nombre}`;
    // if(pais.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(pais);
      }});
    // }
   }

   serviceCambiarEstado(pais: Pais){
       this._servicePais.cambiarEstadoPais(pais.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getPaises();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

}
