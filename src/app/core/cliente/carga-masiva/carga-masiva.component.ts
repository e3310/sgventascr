import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServiceCliente } from '../../../services/cliente.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import * as XLSX from 'xlsx';
import { Pais } from '../../../models/pais';
import { Cliente } from '../../../models/cliente';
// import { CliVentaMes } from '../../../models/cliventames';
import { ConstantsService } from '../../../services/constants.service';
import { ExportXLSX } from 'app/utils/exportXLSX';
import { RepCliVenta } from '../../../models/repCliVenta';
import { isEmpty } from 'rxjs/operators';

@Component({
  selector: 'app-carga-masiva',
  templateUrl: './carga-masiva.component.html',
  styleUrls: ['./carga-masiva.component.scss']
})
export class CargaMasivaComponent implements OnInit {

  _ABREVIATURA= 'CLICM';
  negocioKey = Number(ConstantsService.getNegocioKey());
  file: File;
  fileName = "";
  fileSize = 0;
  fileForm: FormGroup;
  title: string;
  progress: number=0;
  messagesProgress ="";

  // lectura de archivo
  storeData: any;
  fileUploaded: File;
  worksheet: any;
  jsonContent: any;

  //Estructura de archivo
  numKeysArchivo = 5;

  // Control de carga de datos
  archivCargado: boolean;
  errorEncabezado: string;
  estadoReemplazo = 'ANULADO';
  existeError = false;
  errores = [];
  existeErrorGeneral = false;
  existeDuplicadosClientes = false;
  existeDuplicadosVentas = false;
  cargaCorrecta = false;

  // Listas Servicio
  listaPaisesService : Pais[]= [];
  listaClientesService: Cliente[] =[];
  listaVentasClienteService : RepCliVenta[] = [];

  //Listas ControlDuplicados
  listaNombresPaises = [];
  listaCliUsuariosDuplicados = [];
  listaVentasDuplicadas = [];

  // Encabezados obligatorios
  keyPais ='PAIS';
  keyCliUsuario ='USUARIO';
  keyCliCiudad = 'CIUDAD';
  keyCliTelefono = 'TELEFONO';
  keyCliCorreo ='CORREO';
  ListaEncabezados = [this.keyCliUsuario, this.keyCliCiudad, this.keyPais,this.keyCliCorreo]
  // Información Fechas
  hoy:Date = new Date();
  anioActual = this.hoy.getFullYear();
  mesAnterior = this.hoy.getMonth();
  listaMeses = [
    {id: '01', value: 'ENERO'},
    {id: '02', value: 'FEBRERO'},
    {id: '03', value: 'MARZO'},
    {id: '04', value: 'ABRIL'},
    {id: '05', value: 'MAYO'},
    {id: '06', value: 'JUNIO'},
    {id: '07', value: 'JULIO'},
    {id: '08', value: 'AGOSTO'},
    {id: '09', value: 'SEPTIEMBRE'},
    {id: '10', value: 'OCTUBRE'},
    {id: '11', value: 'NOVIEMBRE'},
    {id: '12', value: 'DICIEMBRE'}
  ]

  constructor(private fb: FormBuilder, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any, private _serviceCliente: ServiceCliente) { }

  ngOnInit(): void {
    this.initFileForm();
    this.title = 'Carga Masiva de Clientes';
    console.log('anio actual', this.anioActual)
    console.log('mesAnt',this.mesAnterior);
    
  }

  initFileForm() {
    this.fileForm = this.fb.group({
      file: ['', [Validators.required]]
     });
  }

  public onFileChange(event): void {
    console.log('CARGANDO ARCHIVO',event);
    
    this.limpiarErrores();
    this.archivCargado = false;    
    if (event.target.files.length) {
      this.file = event.target.files[0];
      this.cargarInfoArchivo(this.file);    
      // this.progress = 5;
      this.readExcel();
      
    }
  }

  limpiarErrores(){
    this.errores = [];
    this.existeError = false;
    this.existeErrorGeneral = false;
    this.existeDuplicadosClientes = false;
    this.existeDuplicadosVentas = false;
    this.cargaCorrecta = false;
  }

  cargarInfoArchivo(file: File){
    this.fileName = file.name;
    this.fileSize = file.size/(1024*1024);
  }

  public onUploadClick(): void {
    if (this.file) {      
      if(this.existeError === false){
        this.verificarNumeroEncabezados(this.jsonContent);
        let data = this.llenarDataService();
        console.log(data);        
        this.llenarArraysService(this.jsonContent);   
        if(this.verificarErrores() === false){           
          this.servicioCargaMasiva(data);
        }else{
          this.limpiarArchivo();          
        }
      }else{
        this.limpiarArchivo();
      }
      // this.limpiarListasService();
    }else{
      console.log('Error al subir');
    }
  }

  limpiarArchivo(){
    this.file = undefined;
    // this.fileName='';
    this.fileSize=0;
    this.progress = 0;
    this.messagesProgress ='';
  }

  llenarDataService (){
    let data = {
      paises: this.listaPaisesService,
      clientes: this.listaClientesService,
      ventas: this.listaVentasClienteService
    }
    return data;
  }

  limpiarListasService(){
    this.listaPaisesService = [];
    this.listaClientesService =[];
    this.listaVentasClienteService= [];
  }

  servicioCargaMasiva(data: any){
    this.progress+= 10;
    this.messagesProgress='Guardando información...';
    this._serviceCliente.saveClienteMasivo(data).subscribe( event => {
      // console.log('CARGA MASIVA RESPUESTA',event);      
      // this.limpiarArchivo();
      this.accionesCargaFinalizada();
    },(err: any) => {
          // console.log('ERROR IMPRESO CARGA MASIVA-> ',err);
          if(err['status']===200){
            this.accionesCargaFinalizada();
          }else{
            SweetAlert.show(false,'error',err['error']['message'], 'Error al procesar información', false, true);
            this.limpiarArchivo();
            this.fileForm.reset();
            // this.existeDuplicadosClientes = false;
            // this.existeDuplicadosVentas = false;
            this.storeData = null;
            this.worksheet = null;
            this.jsonContent = null;
            this.limpiarListasService();
          }
        });  

  }

  accionesCargaFinalizada(){
    this.cargaCorrecta = true;
    this.progress = 100;   
    this.messagesProgress ='Carga Finalizada';
    this.fileForm.reset();
  }

  verificarErrores(){    
    let existenErrores = false;
    let erroresGenerales = this.existenErroresGenerales();
    let errorTypeCount = 0;

    if( erroresGenerales === true){ // si existen erroreS generales no verifico duplicados
      errorTypeCount++;
    }else{
      let clientesDuplicados = this.existenClientesDuplicados()
      let ventasDuplicadas = this.existenVentasDuplicadas();
      if(clientesDuplicados === true || ventasDuplicadas === true){
        errorTypeCount++;
      }
    }

    if(errorTypeCount>0){
      existenErrores = true;
      this.existeError = true;
    }


    // if( erroresGenerales === true || clientesDuplicados === true || ventasDuplicadas === true){
    //   existenErrores = true;
    //   this.existeError = true;
    // }     
    return existenErrores;
  }

  existenErroresGenerales(){
    if(this.errores.length >0){
      this.existeErrorGeneral = true;
      return true;
    }else return false;
  }

  existenClientesDuplicados(){    
    if(this.listaCliUsuariosDuplicados.length >0 ){
      this.existeDuplicadosClientes = true;
      return true;
    }else return false;
  }

  existenVentasDuplicadas(){
    if(this.listaVentasDuplicadas.length >0){
      this.existeDuplicadosVentas = true;
      return true;
    }else return false;  
  }



  /**
   * Leer archivo y transformarlo a json
   */
   readExcel() {
    this.storeData = null;
    this.worksheet = null;
    this.jsonContent = null;
    let readFile = new FileReader();
    if(this.file){
    var extension = this.file.name.split('.')[1];
      readFile.onload = (e) => {
        console.log(e);
        
        if(extension.toLocaleLowerCase() === "xlsx"){
          this.storeData = readFile.result;
          var data = new Uint8Array(this.storeData); 
          var arr = new Array();  
          for (var i = 0; i != data.length; ++i) { arr[i] = String.fromCharCode(data[i]);}
          var bstr = arr.join("");
          var workbook = XLSX.read(bstr, { type: 'binary', raw: true});
          var first_sheet_name = workbook.SheetNames[0];
          this.worksheet = workbook.Sheets[first_sheet_name];
          var XL_row_object = XLSX.utils.sheet_to_json(this.worksheet);
          this.jsonContent = XL_row_object;
          console.log('Read Excel', this.jsonContent);
          this.archivCargado = true;
        }else{
          this.existeError = true;
          this.errores.push('Formato de archivo no válido');
          console.log('general','Formato de archivo no válido');
          
          this.limpiarArchivo();
          // SweetAlert.show(false, 'error', 'Formato de archivo no válido', '');
        }
      }
      readFile.readAsArrayBuffer(this.file);
    }else{
      this.existeError = true;
      this.errores.push('No ha seleccionado un archivo');
      
      this.limpiarArchivo();
      // SweetAlert.show(false, 'warning', 'No ha seleccionado un archivo');
    }
  }

    /**
   * Verifica que el número de encabezados sea igual al requerido
   * @param jsonContent   contenido del archivo en formato json
   * @returns boolean
   */
     verificarNumeroEncabezados(jsonContent: any){
      var keys = Object.keys(jsonContent[0]);            
      if (keys.length >= this.numKeysArchivo) {
        return this.verificarFormatoEncabezados(keys);
      } else {
        this.errores.push('Columnas ingresadas: ' + keys.length + '. Columnas requeridas: ' + this.numKeysArchivo);
        return false;
      }
    }

      /**
   * Verifica que todas las columnas requeridas existen
   * @param keys    cabeceras del archivo en formato string de arrays
   * @returns boolean
   */
    verificarFormatoEncabezados(keys: string[]) {      
      let estado = true;
      for (let index = 0; index < this.ListaEncabezados.length; index++) {
          let indice = keys.includes(this.ListaEncabezados[index]);
          if(indice === false){
            estado = false;           
            this.errores.push('El archivo no tiene la columna '+this.ListaEncabezados[index]);
            console.log('general','Formato de archivo no válido');
          }
      }
      return estado;
    }

    llenarArraysService(jsonContent: any){
      console.log('llenar arrrays');      
      let avance = 45;      
      let velocidad = avance/jsonContent.length;
      
      this.jsonContent.forEach( element => {
        this.listaNombresPaises.push(element[this.keyPais]);        
        this.llenarArrayClientes(this.llenarClienteForArrayService(element));
        this.llenarArrayVentasMes(element);
        this.progress+= velocidad;
        this.messagesProgress = 'Transformando información de clientes y ventas';
      });            
      this.llenarArrayPaises(this.listaNombresPaises);
      this.messagesProgress = 'Conversión de archivo a datos finalizada.';
      // console.log('listaClientesDuplicados',this.listaCliUsuariosDuplicados);
      // console.log('listaVentasDuplicadas',this.listaVentasDuplicadas);
    }

    llenarArrayPaises(lista: string[]){
      let avance = 15;      
      let listaUnicos = new Set(lista);
      let velocidad = avance/listaUnicos.size;      
      listaUnicos.forEach( name=> {
        let paisNuevo:Pais = {
          nombre: name
        };
        this.listaPaisesService.push(paisNuevo);
        this.progress= this.progress+ velocidad;        
      });
      this.messagesProgress= 'Información de paises transformada';
      // this.convertirArray(this.listaPaisesService)       
    }


    llenarClienteForArrayService(element: any){
      // if(element[this.keyCliUsuario]!== null && element[this.keyCliUsuario]!== '' ){
        let cliente : Cliente = {
          pais_id: element[this.keyPais],
          usuario_credito: element[this.keyCliUsuario],
          ciudad: element[this.keyCliCiudad],
          telefono: element[this.keyCliTelefono],
          email: element[this.keyCliCorreo],
          neg_id: this.negocioKey
        };
        return cliente;
      // }else return null;
    }

    llenarArrayClientes(cliente: Cliente){
      // if(cliente!== null){
        let buscarDuplicado = this.listaClientesService.find(x=> x.usuario_credito === cliente.usuario_credito);
        if(buscarDuplicado){
          let duplicado = [cliente.usuario_credito];
          this.listaCliUsuariosDuplicados.push(duplicado);
        }else{
          this.listaClientesService.push(cliente);
        }
      // }
    }

    llenarCliVentasForArrayService(usuario: string, mes: string, nroCreditos: number){
      let cliVentas: RepCliVenta = {
        tipo:"M",
        mes: mes,
        anio: this.anioActual.toString(),
        neg_id: this.negocioKey,
        cli_id: usuario,
        nro_creditos: nroCreditos,
        bonificacion: 0        
      };
      return cliVentas;
    }

    llenarArrayVentasMes(element){     
      let keys = Object.keys(element);
      let indexMesAnterior = this.listaMeses.findIndex(x=> parseInt(x.id) === this.mesAnterior);      
      for (let index = 0; index <= indexMesAnterior; index++) {
        let mes = keys.find(x=> x === this.listaMeses[index].value);
        if(mes){          
          let buscarDuplicado = this.listaVentasClienteService.find(x=> x.cli_id === element[this.keyCliUsuario] &&
            x.mes === this.listaMeses[index].id && x.anio === this.anioActual.toString() )
            if(buscarDuplicado){
              let nombreMes = this.listaMeses.find(x=> x.id === buscarDuplicado.mes)
              let duplicado= [buscarDuplicado.cli_id, buscarDuplicado.nro_creditos, nombreMes.value, buscarDuplicado.anio];
              this.listaVentasDuplicadas.push(duplicado);
            }else{
              this.listaVentasClienteService.push(this.llenarCliVentasForArrayService(element[this.keyCliUsuario], this.listaMeses[index].id, element[mes]));          
            }
        }
      }
    }

      /**
   * Descarga errores a un archivo de formato excel
   */
  descargarClientesDuplicados(){
    SweetAlert.show(false, 'info', 'Por favor espere', 'Generando Archivo', true);
    let encabezado = [this.keyCliUsuario ];
    ExportXLSX.downloadXLSX(encabezado, this.listaCliUsuariosDuplicados, 'Usuarios Duplicados');
    SweetAlert.close();
  }

  descargarVentasDuplicadas(){
    SweetAlert.show(false, 'info', 'Por favor espere', 'Generando Archivo', true);
    let encabezado = [this.keyCliUsuario, 'CREDITOS', 'MES', 'AÑO' ];
    ExportXLSX.downloadXLSX(encabezado, this.listaVentasDuplicadas, 'Ventas Duplicadas');
    SweetAlert.close();
  }

    convertirArray(lista: Object[]){
      let arr = [];  
        Object.keys(lista).map(function(key){  
          arr.push({[key]:lista[key]})  
          return arr;  
      });  
      console.log('Object=',lista)  
      console.log('Array=',arr)  
    }



  salir(){
    if(this.cargaCorrecta === true){
      this.matDialogRef.close('SAVE');
    }else{
      this.matDialogRef.close('OK');
    }
  }

}
