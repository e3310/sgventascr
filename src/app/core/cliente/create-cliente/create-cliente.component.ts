import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { Pais } from 'app/models/pais';
import { ServicePais } from '../../../services/pais.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatOption } from '@angular/material/core';
import * as CryptoJS from 'crypto-js';
import { ServiceCliente } from '../../../services/cliente.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConstantsService } from '../../../services/constants.service';
import { Cliente } from '../../../models/cliente';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-create-cliente',
  templateUrl: './create-cliente.component.html',
  styleUrls: ['./create-cliente.component.scss']
})
export class CreateClienteComponent implements OnInit {

  _ABREVIATURA= 'CLIC'
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = ConstantsService.getNegocioKey();
  clienteSelected: Cliente = new Cliente();

  // FormBuilder
  // form: FormGroup;
  submitted = false;
  formValid = true;

  listaPaises: Pais [] = [];
  listaPaisesEncontrados: Pais[] = [];
  isLoading = true;
  filteredPaises: Observable<Pais[]> = new Observable();
  isValidPais = true;  

  listaGeneros = ConstantsService.listaGeneros;

  paisField: FormControl = new FormControl('', [Validators.required]);

  mainForm: FormGroup = new FormGroup({
    pais_id: this.paisField,
    usuario_credito: new FormControl('', [Validators.required, Validators.maxLength(45)]),
    nombres: new FormControl('', [Validators.maxLength(100)]),
    apellidos: new FormControl('', [Validators.maxLength(100)]),
    telefono:  new FormControl('', [Validators.required, Validators.maxLength(15)]),
    ci:  new FormControl('', Validators.maxLength(45)),
    ciudad:  new FormControl('', Validators.maxLength(150)),
    direccion:  new FormControl('', Validators.maxLength(200)),
    email:  new FormControl('', [Validators.email, Validators.maxLength(45)]),
    genero:  new FormControl('')
  });

  constructor(private _servicePais: ServicePais, private _serviceCliente: ServiceCliente, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.accion=parametros.accion;     
      this.listaPaisesEncontrados = parametros.paises; 
      this.clienteSelected = parametros.cliente;
      
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    this.getPaises();
    this.initialize();    
  }
  
  initialize() {
    if (this.accion === 'c') {
      this.title = 'Crear nuevo cliente';
    } else {
      this.title ='Editar cliente';
      this.asignarClienteForm();
    }
    SweetAlert.close();  
  }


  public hasError = (controlName: string, errorName: string) => {
    return this.mainForm.controls[controlName].hasError(errorName);
  }

  asignarClienteCrear() {   
    // let clienteAux = this.mainForm.value;
    // clienteAux.pais_id = clienteAux.pais_id.id;
    // clienteAux.neg_id = clienteAux.pais_id.id;
    // let cliente = clienteAux;
    let cliente : Cliente = {
      usuario_credito: this.mainForm.get('usuario_credito').value,
      nombres : this.mainForm.get('nombres').value,
      apellidos : this.mainForm.get('apellidos').value,
      telefono: this.mainForm.get('telefono').value,
      pais_id: this.mainForm.get('pais_id').value['id'],
      ci: this.mainForm.get('ci').value,
      ciudad: this.mainForm.get('ciudad').value,
      direccion: this.mainForm.get('direccion').value,
      email: this.mainForm.get('email').value,
      genero : this.mainForm.get('genero').value,
      neg_id: Number(this.negocioKey),
      usuario_registro: Number(ConstantsService.getUserKey())
    }
    // if(this.accion === 'c'){
    //   cliente.usuario_registro = 1;
    // }
    console.log(cliente);
    
    return cliente;
  }

  asignarClienteForm() {
    console.log(this.listaPaisesEncontrados);
    this.mainForm.get('usuario_credito').setValue(this.clienteSelected.usuario_credito);
    this.mainForm.get('nombres').setValue(this.clienteSelected.nombres);
    this.mainForm.get('apellidos').setValue(this.clienteSelected.apellidos);
    this.mainForm.get('telefono').setValue(this.clienteSelected.telefono);
    this.mainForm.get('pais_id').setValue(this.cargarPaisSeleccionado(this.clienteSelected.pais_id));
    this.mainForm.get('ci').setValue(this.clienteSelected.ci);
    this.mainForm.get('ciudad').setValue(this.clienteSelected.ciudad);
    this.mainForm.get('direccion').setValue(this.clienteSelected.direccion);
    this.mainForm.get('email').setValue(this.clienteSelected.email);
    this.mainForm.get('genero').setValue(this.clienteSelected.genero);
  }

  serviceCreateCliente(){
    this._serviceCliente.saveCliente(this.asignarClienteCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar();
    
    
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar cliente');
    });
  }

  serviceUpdateCliente(){
    this._serviceCliente.updateCliente(this.asignarClienteCrear(), this.clienteSelected.id).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar cliente');
    });
  }

  guardar(){
    this.submitted = true;
    this.mainForm.markAllAsTouched();
    if (this.mainForm.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        this.serviceCreateCliente();
      } else {
        this.serviceUpdateCliente();
      }
      // this.mainForm.reset();
    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.matDialogRef.close('SAVE');
  }

  getPaises() {
  //   if (this.isLoading) {
  //     SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
  //   }
  //   // this._servicePais.getPaises().subscribe( (result: Pais[]) => {
  //     // this.listaPaisesEncontrados = result;
      this.listaPaisesEncontrados = this.listaPaisesEncontrados;
      this.addFilterPais();           
  //     this.isLoading = false;
  //     SweetAlert.close();
      this.initialize();
  //   // }, (error: HttpErrorResponse) => {
  //   //   console.log(error);
  //   //   SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
  //   // });
  }

  displayPais(pais: Pais): string {
    console.log(pais);    
    return pais ? pais.nombre : '';
  }

  private _filterPais(value: string): Pais[] {
    // this.listaPaisesEncontrados = this.listaPaises.filter(pais => pais.nombre.includes(value))
    // if(value.length >= 2){
    return this.listaPaisesEncontrados.filter(pais => pais.nombre.toUpperCase().includes(value.toUpperCase()));
    // }else {
      // return this.listaPaisesEncontrados;
    // }
  }

  addFilterPais(): void {
    this.filteredPaises = this.paisField.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const val = value.nombre? value.nombre : value;
        const result = this._filterPais(val);
        this.isValidPais = result.length > 0;
        return result;
      }));
  }

  blurPais(): void {
    if (!this.isValidPais) {
      this.paisField.setValue('');
    }
  }

  closedPais(): void {
    let id = this.paisField.value.id;
    if(id){

    }else{
      this.paisField.setValue('');
    }
    // const find = this.listaPaisesEncontrados.find(element => element.id === id);
    
  }
  
  cargarPaisSeleccionado(idPais: number){
    let pais = this.listaPaisesEncontrados.find(x=> x.id === idPais);
    return pais;
  }
}
