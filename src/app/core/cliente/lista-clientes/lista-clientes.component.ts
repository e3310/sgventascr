import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Cliente } from 'app/models/cliente';
import { ServiceCliente } from '../../../services/cliente.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { MatDialog } from '@angular/material/dialog';
import { ConstantsService } from '../../../services/constants.service';
import { CreateClienteComponent } from '../create-cliente/create-cliente.component';
import { ServicePais } from '../../../services/pais.service';
import { Pais } from '../../../models/pais';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
//Importar archivo
import { UploadFileService } from '../../../services/upload-file.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
// import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../../environments/environment.prod';
import { CargaMasivaComponent } from '../carga-masiva/carga-masiva.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Mensajes } from '../../../utils/mensajes';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.scss']
})
export class ListaClientesComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'CLIL';
  _ACCIONES = 'CRUDSM';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaClientes: Cliente [] = [];
  listaTempClientes: Cliente [] = [];
  listaEstados = ['ACTIVO','INACTIVO']
  isLoading = true;
  clienteSeleccionado: Cliente = new Cliente();
  idNegocio = Number(ConstantsService.getNegocioKey());
  listaPaises: Pais [] = [];
  listaPaisesActivos: Pais [] = [];
  isBusquedaActiva = false;

  // FormBuilder de búsqueda
  searchForm: FormGroup;
  submitted = false;
  formValid = true;
  listaPaisesEncontrados: Pais[] = [];
  filteredPaises: Observable<Pais[]> = new Observable();
  isValidPais = true;  
  paisField: FormControl = new FormControl(1, [Validators.required]);
  isVisiblePais = false;
  listaCamposBusqueda =[{id: 'U', value:'Usuario Créditos'},{id: 'N', value:'Nombres'}, {id: 'A', value:'Apellidos'}, {id: 'P', value:'Pais'}];
  criterioBusqueda = 'Usuario Créditos';

  //Información MatTable
  dataSource: MatTableDataSource<Cliente> = new MatTableDataSource<Cliente>(this.listaClientes);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  displayedColumns: string[] = [
    'acciones',
    'usuario',
    'telefono',
    'nombres',
    'apellidos',
    'estado',
    'pais'
  ];
  // @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator =  ViewChild(MatPaginator);
  @ViewChild('paginator', { static: false }) set paginator(cliente: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && cliente !== undefined ){
      this.dataSource.paginator = cliente;
      cliente._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      cliente._intl.previousPageLabel = this.mensajes.itemAnterior;
      cliente._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  
  constructor(private _serviceCliente: ServiceCliente, private matDialog: MatDialog, private _servicePais: ServicePais,
    private fb: FormBuilder,private mensajes: Mensajes) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
   }
   
   initSearchForm() {
    this.searchForm = this.fb.group({
      valorBusqueda: ['', [Validators.required, Validators.maxLength(100)]],
      campoBusqueda : ['U', [Validators.required, Validators.maxLength(1)]], 
      pais_id: this.paisField
     });
  }

   ngOnInit():void{
     if( this.negocioKeySesion && this.negocioValueSesion
      && this._PERMISO.isAuthorized.state === true){       
        this.getClientes();
     }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

   getClientes() {     
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de clientes', true, false);
    }
    this._serviceCliente.getClientes(this.idNegocio).subscribe( (result: Cliente[]) => {
      this.listaClientes = result;
      this.listaTempClientes = result;
      this.dataSource.data = result;
      SweetAlert.close();
      this.getPaises();
      this.getPaisesActivos();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  crearElementoModal(accion: string,cliente?: Cliente){
    return {
      accion,
      cliente: cliente ? cliente: null,
      paises: this.listaPaisesActivos
    }
  }


  actionCreate(accion: string,cliente?: Cliente){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, cliente)
    }
    const dialogRef = this.matDialog.open(CreateClienteComponent, {    
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);      
      if(data === 'SAVE'){
        this.getClientes();
      }
    });
  }

  cambiarEstado(cliente: Cliente){
    let mensaje =`Se cambiará el estado del cliente ${cliente.nombres} ${cliente.apellidos}`;
    // if(cliente.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(cliente);
      }});
    // }
   }

   serviceCambiarEstado(cliente: Cliente){
       this._serviceCliente.cambiarEstadoCliente(cliente.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        if(this.isBusquedaActiva === true){
          this.actionSearch();
        }else{
          this.getClientes();
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

   getPaises() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información Paises', true, false);
    }
    this._servicePais.getPaises().subscribe( (result: Pais[]) => {
      this.listaPaises = result;       
      this.isLoading = false;
      this.addFilterPais(); 
      SweetAlert.close();      
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  getPaisesActivos() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información Paises', true, false);
    }
    this._servicePais.getPaisesActivos().subscribe( (result: Pais[]) => {
      this.listaPaisesActivos = result;       
      this.isLoading = false;
      SweetAlert.close();
      
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  cargarNombrePais(idPais: number){
    let pais = this.listaPaises.find(x=> x.id === idPais);
    if(pais){
      return pais.nombre;
    }else{
      return null;
    }
  }

  onChangeEvent(event: any){
    console.log(event.target.value);
  }

  actionViewFormSearch(){
    this.initSearchForm();  
    this.isBusquedaActiva = true;
  }

  actionHideFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
    this.isBusquedaActiva = false;    
    this.listaClientes = [...this.listaTempClientes];
    this.dataSource.data = this.listaClientes;
    // this.ngxDatatable.element.click();
    this.initSearchForm();
    this.actionSearchPais('U');
    SweetAlert.close();
  }

  actionRadioChange(mrChange: MatRadioChange){
    let aux= this.listaCamposBusqueda.find(x=> x.id === mrChange.value);
    if(aux !== null){
      this.criterioBusqueda = aux.value;
      this.listaClientes = [...this.listaTempClientes];
      this.dataSource.data = this.listaClientes;
      this.searchForm.controls['valorBusqueda'].setValue('');
      this.actionSearchPais(aux.id);
    }else{
      this.criterioBusqueda = 'Nombres aux';
    }
  }

  actionSearchPais(id: string){
    if(id === 'P'){
      this.isVisiblePais = true;
      this.paisField.setValue('');
      this.searchForm.get('valorBusqueda').setValue('S/I');
    }else{
      this.isVisiblePais = false;
      this.paisField.setValue(0);
    }

  }

  actionSearch(){
    this.submitted = true;
    this.searchForm.markAllAsTouched();
    if(this.searchForm.valid){
      this.formValid = true;
      this.isLoading = true;
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
      let valorBusqueda=this.searchForm.get('campoBusqueda').value !='P'? this.searchForm.get('valorBusqueda').value: this.paisField.value['id'];
      console.log('valor busqueda', valorBusqueda);
      
      this._serviceCliente.searchClientes(this.idNegocio,this.searchForm.get('campoBusqueda').value,valorBusqueda).subscribe( (result: Cliente[]) => {
        this.listaClientes =[...result];
        this.dataSource.data = result;
        SweetAlert.close();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
      });
      this.isLoading = false;
    }else{
      this.formValid = false;
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.searchForm.controls[controlName].hasError(errorName);
  }


  //Subir archivos
  actionCargaMasiva(){
    let datos = null;
    const dialogRef = this.matDialog.open(CargaMasivaComponent, {
      // width: '90%',
      // height: '90%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);      
      if(data === 'SAVE'){
        this.getClientes();
      }
    });
  }

  // Select pais
  displayPais(pais: Pais): string {
    console.log(pais);    
    return pais ? pais.nombre : '';
  }

  private _filterPais(value: string): Pais[] {
    return this.listaPaisesActivos.filter(pais => pais.nombre.toUpperCase().includes(value.toUpperCase()));
  }

  addFilterPais(): void {
    this.filteredPaises = this.paisField.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const val = value.nombre? value.nombre : value;
        const result = this._filterPais(val);
        this.isValidPais = result.length > 0;
        return result;
      }));
  }

  blurPais(): void {
    if (!this.isValidPais) {
      this.paisField.setValue('');
    }
  }

  closedPais(): void {
    let id = this.paisField.value.id;
    if(id){

    }else{
      this.paisField.setValue('');
    }
    // const find = this.listaPaisesEncontrados.find(element => element.id === id);
    
  }


}
