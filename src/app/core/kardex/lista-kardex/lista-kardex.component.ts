import { Component, OnInit, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { AppDateAdapter, MY_FORMATS } from '../../directives/format-datepicker';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { PdfMake } from '../../../utils/pdfMake';
import { Kardex } from 'app/models/kardex';
import { ExportXLSX } from '../../../utils/exportXLSX';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { ServiceKardex } from '../../../services/kardex.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { DatePipe } from '@angular/common';
import { ConstantsService } from '../../../services/constants.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CreateCompraComponent } from '../../compra/create-compra/create-compra.component';
import { CreateVentaComponent } from '../../venta/create-venta/create-venta.component';

@Component({
  selector: 'app-lista-kardex',
  templateUrl: './lista-kardex.component.html',
  styleUrls: ['./lista-kardex.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class ListaKardexComponent implements OnInit {


  // _ABREVIATURA= 'KARL';
  diasValidos = 3;
  negocioValueSesion = ConstantsService.getNegocioValue();
  negocioKey = Number(ConstantsService.getNegocioKey());
  listaKardex: Kardex [] = [];
  listaTempKardex: Kardex [] = [];
  listaCamposBusqueda =[{id: 'F', value:'Rango de fechas'},{id: 'M', value:'Tipo Movimiento'}];
  criterioBusqueda = 'Rango de fechas';

  nroVentas = 0;
  listaEstados = ConstantsService.listaEstados
  listaTiposCuenta = ConstantsService.listaTiposCuenta;

  isLoading = true;
  kardexSeleccionado: Kardex = new Kardex();
  idNegocio = Number(ConstantsService.getNegocioKey());
  //Información MatTable
  dataSource: MatTableDataSource<Kardex> = new MatTableDataSource<Kardex>(this.listaKardex);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator =  ViewChild(MatPaginator);
  displayedColumns: string[] = [
    'acciones',
    // 'id',
    'fechaRegistro',    
    'cantidad',
    'stock',
    'costo',
    'total_detalle',   
    'movimiento',
    'estado'
  ];

  // FormBuilder de búsqueda
  searchForm: FormGroup;
  submitted = false;
  formValid = true;
  isBusquedaActiva = false;
  fechaInicioField = new FormControl(new Date(),Validators.required);
  fechaFinField = new FormControl(new Date(),Validators.required);
  movField = new FormControl(0, [Validators.required]);
  listaMovimientos = ConstantsService.listaMovimientosKardex;
  //Alternar input
  isVisibleFecha = true;
  isVisibleMovimiento = false;
  valor1 = null;
  valor2 = null;
  campoBusqueda = 'F';

  // FormBuilder de recalculo
  recalculoForm: FormGroup;
  submittedRec = false;
  formValidRec = true;
  isRecalculoActivo = false;
  fechaRecalculoField = new FormControl(new Date(),Validators.required);

  //Checbox descarga
  isVisualizar = false;
  isDescargar = true;
  listaKardexSearch: Kardex [] = [];

  currentDay : Date;
  firstDayMonth : Date;
  lastDayMonth : Date;
  minDate : Date;

  constructor(private _serviceKardex: ServiceKardex,
    private matDialog: MatDialog, private mensajes: Mensajes, public datepipe: DatePipe,
    private fb: FormBuilder) {
      this.currentDay = new Date();
      this.firstDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth(), 1);
      this.lastDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth() + 1, 0);
      this.minDate = new Date(2021,0,1);
   }

  ngOnInit():void{
    this.getKardex();
    // this.getClientes();
  }

  getKardex() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de movimientos', true, false);
    }
    this._serviceKardex.getKardex(this.idNegocio).subscribe( (result: Kardex[]) => {
      this.listaKardex = result;
      this.listaTempKardex = result;
      this.asignarMatTable(this.listaKardex);
      SweetAlert.close();
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(ventas: Kardex[]){
    this.dataSource.data = ventas;
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string, kardex?: any){
    return {
      accion,
      kardex: kardex ? kardex: null
    }
  }

  serviceShowMovOrigen(idKardex: number, tipoMov: string){
    this._serviceKardex.showMovimiento(this.idNegocio,idKardex,tipoMov).subscribe( (result) => {
      let datos = this.crearElementoModal('S', result[0]);
      let dialogRef = this.createModal(datos, tipoMov);
    // dialogRef.afterClosed().subscribe(data => {      
    //   // this.getVentas();
    // });
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  actionShow(idKardex: number, tipoMov: string,){    
    let nombreMov = this.listaMovimientos.find(x=> x.id === tipoMov).value;
    SweetAlert.show(false, 'info', 'Espere un momento', `Cargando datos de ${nombreMov.toLowerCase()}`, true, false);
    this.serviceShowMovOrigen(idKardex, tipoMov);
  }

  createModal(datos: any, tipoMov: string){
    let dialogRef : any;
    if(tipoMov === 'C'){
      dialogRef = this.matDialog.open(CreateCompraComponent, {
        // width: '50%',
        // height: '70%',
        disableClose: true,
        data: datos
      });
    }else if(tipoMov === 'V'){
      dialogRef = this.matDialog.open(CreateVentaComponent, {
        // width: '50%',
        // height: '70%',
        disableClose: true,
        data: datos
      });
    }
    return dialogRef;
  }

  

   // BUSCAR VENTAS
   initSearchForm() {
    this.searchForm = this.fb.group({
      campoBusqueda : new FormControl('F', [Validators.required, Validators.maxLength(1)]),
      inicio: this.fechaInicioField,
      fin: this.fechaFinField,
      mov: this.movField,
      // cliente: this.clienteField,
      // paquete: this.paqueteField,
      visualizar: new FormControl(false),
      descargar: new FormControl(true)
     });
  }

  actionViewFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    this.isBusquedaActiva = true;
    this.initSearchForm();    
    SweetAlert.close();
  }

  actionHideFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    this.isBusquedaActiva = false;
    this.listaKardex = [...this.listaTempKardex];
    this.dataSource.data = this.listaKardex;
    this.initSearchForm();
    SweetAlert.close();
  }

  actionRadioChange(mrChange: MatRadioChange){
    let aux= this.listaCamposBusqueda.find(x=> x.id === mrChange.value);
    if(aux !== null){
      this.criterioBusqueda = aux.value;
      this.listaKardex = [...this.listaTempKardex];
      this.dataSource.data = this.listaKardex;
      // this.searchForm.controls['valorBusqueda'].setValue('');
      this.actionSearchCriterio(aux.id);      
    }else{
      this.criterioBusqueda = 'Nombres aux';
    }
  }

  actionSearchCriterio(id: string){
    this.campoBusqueda = id;
    switch(id){
      case 'F':
        this.asignarValoresVisuales(1);
        this.movField.setValue(0);
        break;
      case 'M':
        this.asignarValoresVisuales(2);
        this.restartFieldsFechas();
        this.movField.setValue('');
        break;
    }
  }

  asignarValoresVisuales(posicionVerdadero: number){
    // 'use strict';
    this.isVisibleFecha = false, this.isVisibleMovimiento = false;
    switch(posicionVerdadero){
      case 1:
        this.isVisibleFecha = true;
        break;
      case 2:
        this.isVisibleMovimiento = true;
        break;
    }
  }

  restartFieldsFechas(){
    this.fechaInicioField.setValue(new Date());
    this.fechaFinField.setValue(new Date());
  }

  asignarValoresporCheckBox(field: string, valor1: any, valor2 = 'vacio'){
    this.valor1 = valor1;
    this.valor2 = valor2;
    this.campoBusqueda = field;
  }

  asignarValoresRequest(){
    this.limpiarFieldsRequest();
    switch(this.campoBusqueda){
      case 'F':
        let inicio = this.searchForm.get('inicio').value? this.searchForm.get('inicio').value: Date.now();
        let fin = this.searchForm.get('fin').value? this.searchForm.get('fin').value: Date.now();
        this.asignarValoresporCheckBox(this.campoBusqueda, this.datepipe.transform(inicio, 'yyyy-MM-dd'), this.datepipe.transform(fin, 'yyyy-MM-dd'));
        break;
      case 'M':
        this.asignarValoresporCheckBox(this.campoBusqueda,this.movField.value);
        break;
    }    
  }

  limpiarFieldsRequest(){
    this.valor1 = null;
    this.valor2 = null;
  }

  actionSearch(tipo: string){
    this.submitted = true;
    this.searchForm.markAllAsTouched();
    this.listaKardexSearch = [];
    if(this.searchForm.valid){
      this.formValid = true;
      this.isLoading = true;
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
      this.serviceSearch(tipo);
      this.isLoading = false;
    }else{
      this.formValid = false;
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.searchForm.controls[controlName].hasError(errorName);
  }

  actionChange(checked: boolean, accion: string){
    if(accion === 'v' && checked){
      this.searchForm.controls['descargar'].setValue(false);
      this.isVisualizar = true;
      this.isDescargar = false;
    }else if(accion === 'd' && checked){
      this.searchForm.controls['visualizar'].setValue(false);
      this.isVisualizar = false;
      this.isDescargar = true;
    }
  }

  actionSearchVisualizar(result: Kardex[]){
    this.listaKardex =[...result];
    this.dataSource.data = result;
  }

  actionSearchDescargar(result: Kardex[], tipo: string){
    let reporte = [];
      result.forEach((e: Kardex) => {
        let estado = this.listaEstados.find(x=> x.substr(0,1));
        let item = [ e.fechaRegistro, e.cantidad, e.stock, 
        e.costo, e.total_detalle, e.movimiento, estado];
          reporte.push(item);
      });
      if  (reporte.length > 0){
        let encabezado = ['FECHA REGISTRO', 'CANTIDAD', 'STOCK', 
        'COSTO', 'TOTAL', "MOVIMIENTO","ESTADO"];
        switch(tipo){
          case 'p':
            this.descargarReportePdf(encabezado,reporte);
            break;
          case 'e':
            this.descargarReporteExcel(encabezado,reporte);
            break;
        }
        SweetAlert.close();
      }else{
        SweetAlert.show(false, 'warning', 'No se encontraron resultados para la búsqueda', 'No se ha generado el archivo', false, true);
      }

  }

  descargarReporteExcel(encabezado: any,lista: any[]){
    ExportXLSX.downloadXLSX(encabezado, lista, 'Kardex_por_fecha');
    // SweetAlert.close();
  }

  descargarReportePdf(encabezado: any,lista: any[]) {
    var pdfGenerator =
    PdfMake.createPdf(encabezado, lista, 'Kardex por fecha', 'Administrador', 'landscape');
    PdfMake.downloadPdf(pdfGenerator, 'Kardex_por_fecha');
  }

  serviceSearch(tipo: string){
    this.asignarValoresRequest();
    this._serviceKardex.searchKardex(this.idNegocio,this.valor1,this.valor2, this.campoBusqueda).subscribe( (result: Kardex[]) => {
      if(this.searchForm.controls['visualizar'].value === true){
      this.actionSearchVisualizar(result);
      }else{
        this.actionSearchDescargar(result, tipo);
      }
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  // RECALCULO KARDEX

  initRecalculoForm(){
    this.recalculoForm = this.fb.group({
      fechaInicio: this.fechaRecalculoField
    });
  }
  
  public hasErrorRecalculoForm = (controlName: string, errorName: string) => {
    return this.recalculoForm.controls[controlName].hasError(errorName);
  }


  actionViewFormRecalculo() {        
    this.fechaRecalculoField.setValue(new Date());
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    this.initRecalculoForm();
    this.isRecalculoActivo = true;
    SweetAlert.close();
  }
  

  actionHideFormRecalculo(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    this.isRecalculoActivo = false;
    this.listaKardex = [...this.listaTempKardex];
    this.dataSource.data = this.listaKardex;
    this.initRecalculoForm();
    SweetAlert.close();
  }

  actionRecalculo(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando stocks', true, false);
    this.serviceRecalculoStock();
  }

  serviceRecalculoStock(){
    let inicio= this.fechaRecalculoField.value;
    let fechaNueva= this.datepipe.transform(inicio, 'yyyy-MM-dd');    
    this._serviceKardex.recalculoKardex(this.idNegocio, fechaNueva).subscribe( result => {
      console.log('RECALCULO',result);
      // this.getKardex();
      SweetAlert.show(false, 'info', 'Las cantidades se han actualizado.', 'Recálculo Finalizado', false, true);
    }, (error: HttpErrorResponse) => {
      console.log(error);
      if(error['status']===403){
        SweetAlert.show(false, 'error', `Puede recalcular movimientos de ${this.diasValidos} dias anteriores a la fecha.` ,
         'Imposible recalcular.');
      }else{
          SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible recalcular');
      }
    });
  }

}
