import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaKardexComponent } from './lista-kardex.component';

describe('ListaKardexComponent', () => {
  let component: ListaKardexComponent;
  let fixture: ComponentFixture<ListaKardexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaKardexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaKardexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
