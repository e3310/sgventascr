import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaqueteProducto } from '../../../models/paqueteProducto';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { ServicePaqueteProducto } from '../../../services/paqueteProducto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-paquete',
  templateUrl: './create-paquete.component.html',
  styleUrls: ['./create-paquete.component.scss']
})
export class CreatePaqueteComponent implements OnInit {

  _ABREVIATURA= 'PAQC';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = ConstantsService.getNegocioKey();
  paqueteSelected: PaqueteProducto = new PaqueteProducto();
  idProducto = null;

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;
  isLoading = true;
  isValidPais = true;  

  listaGeneros = ConstantsService.listaGeneros;

  constructor(private fb: FormBuilder, private _servicePaquete: ServicePaqueteProducto, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.initValidators();
      this.accion=parametros.accion;
      this.paqueteSelected = parametros.paquete;
      this.idProducto = parametros.idProducto;
    }

 
    ngOnInit(): void {
      SweetAlert.show(false, 'info', 'Cargando información', '', true);
      this.initialize();    
    }
    
    initialize() {
      if (this.accion === 'c') {
        this.title = 'Crear nuevo paquete';
      } else {
        this.title ='Editar paquete';
        this.asignarPaqueteProductoForm();
      }
      SweetAlert.close();  
    }

    initValidators() {
      this.form = this.fb.group({
        descripcion: ['', [Validators.required, Validators.maxLength(150)]],
        // prod_id: [this.idProducto, [Validators.required]],
        nro_unidades: ['', [Validators.required, Validators.min(1)]],
        precio_venta: ['', [Validators.required]]
       });
    }
  
  
    public hasError = (controlName: string, errorName: string) => {
      return this.form.controls[controlName].hasError(errorName);
    }
  
    asignarPaqueteProductoCrear() {   
      let paquete: PaqueteProducto = this.form.value as unknown as PaqueteProducto;
      paquete.prod_id = this.idProducto;
      paquete.neg_id =parseInt(this.negocioKey);
      // let paqueteAux = this.form.value;
      // paqueteAux.pais_id = paqueteAux.pais_id.id;
      // paqueteAux.neg_id = paqueteAux.pais_id.id;
      // let paquete = paqueteAux;
      // let paquete : PaqueteProducto = {
      //   usuario_credito: this.form.get('usuario_credito').value,
      //   nombres : this.form.get('nombres').value,
      //   apellidos : this.form.get('apellidos').value,
      //   telefono: this.form.get('telefono').value,
      //   pais_id: this.form.get('pais_id').value['id'],
      //   ci: this.form.get('ci').value,
      //   ciudad: this.form.get('ciudad').value,
      //   direccion: this.form.get('direccion').value,
      //   email: this.form.get('email').value,
      //   genero : this.form.get('genero').value,
      //   neg_id: Number(this.negocioKey)
      //   // usuario_registro:1
      // }
      // // if(this.accion === 'c'){
      // //   paquete.usuario_registro = 1;
      // // }
      console.log(paquete);
      
      return paquete;
    }
  
    asignarPaqueteProductoForm() {
      this.form.patchValue(this.paqueteSelected);
      // this.form.get('usuario_credito').setValue(this.paqueteSelected.usuario_credito);
      // this.form.get('nombres').setValue(this.paqueteSelected.nombres);
      // this.form.get('apellidos').setValue(this.paqueteSelected.apellidos);
      // this.form.get('telefono').setValue(this.paqueteSelected.telefono);
      // this.form.get('pais_id').setValue(this.cargarPaisSeleccionado(this.paqueteSelected.pais_id));
      // this.form.get('ci').setValue(this.paqueteSelected.ci);
      // this.form.get('ciudad').setValue(this.paqueteSelected.ciudad);
      // this.form.get('direccion').setValue(this.paqueteSelected.direccion);
      // this.form.get('email').setValue(this.paqueteSelected.email);
      // this.form.get('genero').setValue(this.paqueteSelected.genero);
    }
  
    serviceCreatePaqueteProducto(){
      this._servicePaquete.savePaquete(this.asignarPaqueteProductoCrear()).subscribe(result =>{
        console.log(result);
        this.accionesGuardar();      
      
      }, error => {
        console.log(error);
        SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar paquete');
      });
    }
  
    serviceUpdatePaqueteProducto(){
      this._servicePaquete.updatePaquete(this.asignarPaqueteProductoCrear(), this.paqueteSelected.id).subscribe(result =>{
      this.accionesGuardar();
      }, (error ) => {
        SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar paquete');
      });
    }
  
    guardar(){
      this.submitted = true;
      this.asignarPaqueteProductoCrear();
      console.log('FORM',this.form);
      
      this.form.markAllAsTouched();
      if (this.form.valid) {
        this.formValid = true;
        if (this.accion === 'c') {
          // this.asignarPaqueteProductoCrear();
          this.serviceCreatePaqueteProducto();
        } else {
          this.serviceUpdatePaqueteProducto();
        }
        // this.form.reset();
      }else{
        this.formValid = false;
      }
    }
  
    salir(){
      this.matDialogRef.close('OK');
    }
  
    accionesGuardar() {
      SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
      this.matDialogRef.close('SAVE');
    }

}
