import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePaqueteComponent } from './create-paquete.component';

describe('CreatePaqueteComponent', () => {
  let component: CreatePaqueteComponent;
  let fixture: ComponentFixture<CreatePaqueteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatePaqueteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePaqueteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
