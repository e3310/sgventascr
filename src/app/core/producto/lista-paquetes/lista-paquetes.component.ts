import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { PaqueteProducto } from '../../../models/paqueteProducto';
import { Producto } from '../../../models/producto';
import { ServiceProducto } from '../../../services/producto.service';
import { ServicePaqueteProducto } from 'app/services/paqueteProducto.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { CreatePaqueteComponent } from '../create-paquete/create-paquete.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ParametrizarProductoComponent } from '../parametrizar-producto/parametrizar-producto.component';
import { Router } from '@angular/router';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-paquetes',
  templateUrl: './lista-paquetes.component.html',
  styleUrls: ['./lista-paquetes.component.scss']
})
export class ListaPaquetesComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'PAQL';
  _ACCIONES = 'CRUDP';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaProductos: Producto [] = [];
  isVaciaListaProd : boolean;
  isVaciaListaPaq : boolean;
  listaPaquetes: PaqueteProducto [] = [];
  listaEstados = ['ACTIVO','INACTIVO']
  isLoading = true;
  // paqueteSeleccionado: PaqueteProducto = new PaqueteProducto();
  idNegocio = Number(ConstantsService.getNegocioKey());
  // listaPaises: Pais [] = [];
  // isBusquedaActiva = false;

  panelOpenState = true;
  stock: number;
  // FormBuilder para producto
  form: FormGroup;
  formPaq: FormGroup;

  // private productoDiffer: KeyValueDiffer<string, any>;
  private productoCredito: Producto;

  // @ViewChild('divProductos') divProductos: HTMLElement;

  constructor(private _serviceProducto: ServiceProducto, private _servicePaquete: ServicePaqueteProducto, private matDialog: MatDialog,
    private fb: FormBuilder, private router: Router) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
      this.initFormProducto();
      this.initFormPaquete();
      console.log('cons num', this.isVaciaListaProd);
    
  }
  
  ngOnInit(): void {  
    if(this._PERMISO.isAuthorized.state === true){
      this.getProductos();
      console.log('init num', this.isVaciaListaProd);
    // this.getNumeroProductos();
    // this.getPaquetes();
    }
  }

  ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

  initFormProducto() {
    this.form = this.fb.group({
      costo: [{value:'',disabled:true}],
      stock:[{value:'',disabled:true}]
     });
  }

  initFormPaquete() {
    this.formPaq = this.fb.group({
      cantidad: ['']
     });
  }

  getControlLabel(type: string){
    return this.form.controls[type].value;
   }

  // productoChanged(changes: KeyValueChanges<string, any>) {
  //   console.log('changes');
  //   /* If you want to see details then use
  //     changes.forEachRemovedItem((record) => ...);
  //     changes.forEachAddedItem((record) => ...);
  //     changes.forEachChangedItem((record) => ...);
  //   */
  // }

  // ngDoCheck(): void {
  //     const changes = this.productoDiffer.diff(this.productoCredito);
  //     if (changes) {
  //       this.productoChanged(changes);
  //     }
  // }



  getProductos() {
    // if (this.isLoading) {
    //   SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de créditos', true, false);
    // }
    this._serviceProducto.getProductos(this.idNegocio).subscribe( (result: Producto[]) => {
      this.listaProductos = result;
      if(this.listaProductos.length>0){
        this.productoCredito = result[0];
        this.stock= this.productoCredito.stock;
        this.form.patchValue(this.productoCredito);
        this.form.touched;        
        this.isVaciaListaProd = false;
        this.getPaquetes();
        console.log('', this.isVaciaListaProd);
      }else{
        this.isVaciaListaProd = true;
        SweetAlert.show(false, 'info', 'No existe información, debe parametrizarla', 'Información de créditos vacía',false, true);
      }
      
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  getNumeroProductos() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de productos', true, false);
    }
    this._serviceProducto.getProductosCount(this.idNegocio).subscribe( result => {
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  getPaquetes() {
    // if (this.isLoading) {
    //   SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de paquetes', true, false);
    // }
    this._servicePaquete.getPaquetes(this.idNegocio).subscribe( result => {
      this.listaPaquetes = result;
      if(this.listaPaquetes.length >0){
        this.formPaq.controls['cantidad'].setValue(this.listaPaquetes.length);
        this.isVaciaListaPaq = false;
      }else{
        this.isVaciaListaPaq = true;
      }

      // this.divProductos.click();
      // SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  crearElementoModal(accion: string, paquete?: PaqueteProducto){
    return {
      accion,
      paquete: paquete ? paquete: null,
      idProducto: this.productoCredito.id
    }
  }


  actionCreate(accion: string,paquete?: PaqueteProducto){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, paquete)
    }
    const dialogRef = this.matDialog.open(CreatePaqueteComponent, {    
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);      
      if(data !== 'OK'){
        this.ngOnInit();
      }
    });
  }

  actionParametrizar(){
    const dialogRef = this.matDialog.open(ParametrizarProductoComponent, {    
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);      
      if(data !== 'OK'){
        this.productoCredito = data;
        this.refreshPage();
      }
    });
  }

  cambiarEstado(paquete: PaqueteProducto){
    let mensaje =`Se cambiará el estado del paquete ${paquete.descripcion}`;
    // if(paquete.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(paquete);
      }});
    // }
   }

   serviceCambiarEstado(paquete: PaqueteProducto){
       this._servicePaquete.cambiarEstadoPaquete(paquete.id).subscribe( result => {
         console.log(result);         
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
          this.getPaquetes();        
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }
   refreshPage(){    
    this.router.routeReuseStrategy.shouldReuseRoute = ()=> false;
    this.router.onSameUrlNavigation ='reload';
    this.router.navigate(['/core/producto/lista']);   
    window.location.reload(); 
  }


}
