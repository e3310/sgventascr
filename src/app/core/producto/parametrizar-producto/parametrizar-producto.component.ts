import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Producto } from '../../../models/producto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceProducto } from '../../../services/producto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';

@Component({
  selector: 'app-parametrizar-producto',
  templateUrl: './parametrizar-producto.component.html',
  styleUrls: ['./parametrizar-producto.component.scss']
})
export class ParametrizarProductoComponent implements OnInit {

  _ABREVIATURA= 'PROP';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  title: string;
  productoSelected: Producto = new Producto();

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;

  constructor(private fb: FormBuilder, private _serviceProducto: ServiceProducto, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) {console.log(parametros)
      // this.productoSelected = parametros.producto;
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    this.initValidators();
    this.initialize();
  }

  initialize() {
      this.title = 'Parametrizar valores de créditos';
      this.form.reset();     
    SweetAlert.close();
  }

  initValidators() {
    this.form = this.fb.group({
      costoTotal: ['', [Validators.required, Validators.min(1)]],
      costo: ['', [Validators.required]],
      stock: ['', [Validators.required, Validators.min(1)]],
      neg_id: ConstantsService.getNegocioKey(),
      precio:0
     });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarProductoService() {
    let producto: Producto = this.form.value as unknown as Producto;
    // let producto : Producto = {
    //   nombre : this.form.get('nombre').value
    //   // usuario_registro: ConstantsService.decrypt(sessionStorage.getItem('userKey'))
    // }
    producto.neg_id= parseInt(ConstantsService.getNegocioKey());
    producto.costo= Math.round((producto['costoTotal']/producto.stock)*10000)/10000;
    console.log(producto);
    
    return producto;
  }

  getCostoUnitario(){
    console.log('ejecutando cu');
    
    let unitario=0;
    if(this.form.controls['costoTotal'].value !== '' && this.form.controls['stock'].value !== ''){
      let costoTotal = parseFloat(this.form.controls['costoTotal'].value);
      let stock = parseInt(this.form.controls['stock'].value );
      unitario = Math.round((costoTotal/stock)*10000)/10000;
    }
    this.form.controls['costo'].setValue(unitario);
  }

  serviceParametrizarProducto(){
    this._serviceProducto.saveProducto(this.asignarProductoService()).subscribe(result =>{
      this.productoSelected= result['data'];
      console.log(this.productoSelected);
      
      this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar producto');
    });
  }

  guardar(){
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.formValid = true;
      // this.asignarProductoService();
      this.serviceParametrizarProducto();
    }else{
      this.formValid = false;
    }
  }

  salir(){
    if(this.submitted=== true && this.formValid === true){
      this.matDialogRef.close(this.productoSelected);
    }else{
      this.matDialogRef.close('OK');
    }
    
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Producto Parametrizado', '', false, false,1000);
    this.salir();
  }
}
