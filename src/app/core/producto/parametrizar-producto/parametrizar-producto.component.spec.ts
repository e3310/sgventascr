import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrizarProductoComponent } from './parametrizar-producto.component';

describe('ParametrizarProductoComponent', () => {
  let component: ParametrizarProductoComponent;
  let fixture: ComponentFixture<ParametrizarProductoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParametrizarProductoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrizarProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
