import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Usuario } from '../../../models/usuario';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ServiceUsuario } from '../../../services/usuario.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { ServiceVendedor } from '../../../services/vendedor.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { CreateUsuarioComponent } from '../../usuario/create-usuario/create-usuario.component';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-vendedores',
  templateUrl: './lista-vendedores.component.html',
  styleUrls: ['./lista-vendedores.component.scss']
})
export class ListaVendedoresComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'VDL';
  _ACCIONES = 'CRUD';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaVendedores: Usuario [] = [];
  nroCuentasBancarias = 0;
  listaEstados = ConstantsService.listaEstados
  listaTiposCuenta = ConstantsService.listaTiposCuenta;
  isLoading = true;
  formaPagoSeleccionada: Usuario = new Usuario();
  idNegocio = Number(ConstantsService.getNegocioKey());
  tipoVendor = ConstantsService.typeUserVendor;

  //Información MatTable
  dataSource: MatTableDataSource<Usuario> = new MatTableDataSource<Usuario>(this.listaVendedores);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(vendedor: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && vendedor !== undefined ){
      this.dataSource.paginator = vendedor;
      vendedor._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      vendedor._intl.previousPageLabel = this.mensajes.itemAnterior;
      vendedor._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',    
    'cedula',    
    'nombres',
    'apellidos',
    'estado',
    'genero',
    'telefono',
    'email',
    'direccion',
    'nameUsuario'
  ];
  constructor(private _serviceUsuario: ServiceUsuario,private _serviceVendedor: ServiceVendedor, private matDialog: MatDialog, 
    private mensajes: Mensajes) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
    }

   ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getVendedores();  
    }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

   getVendedores() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de vendedores', true, false);
    }
    this._serviceUsuario.getUsersVendedor(this.idNegocio).subscribe( (result: Usuario[]) => {
      this.listaVendedores = result;
      this.asignarMatTable(this.listaVendedores);
      // this.ngxDatatable.element.click();
      console.log(this.listaVendedores)
      this.nroCuentasBancarias = this.listaVendedores.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(vendedores: Usuario[]){
    this.dataSource.data = vendedores;
    // this.dataSource.paginator = this.paginator;
    // this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }


  crearElementoModal(accion: string,tipoUsuario: string,usuario?: Usuario){
    return {
      accion,
      tipoUsuario,
      elemento: 'vendedor',
      usuario: usuario ? usuario: null
    }
  }

  actionCreate(accion: string, tipoUsuario: string,usuario?: Usuario){
    console.log('USUARIO DESDE VENDEDOR', usuario);
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion, tipoUsuario);
    }else{
      datos = this.crearElementoModal(accion, tipoUsuario, usuario)
    }
    console.log(accion, datos);
    
    const dialogRef = this.matDialog.open(CreateUsuarioComponent, {
      width: '50%',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getVendedores();
    });
  }

  cambiarEstado(usuario: Usuario){
    let mensaje =`Se cambiará el estado del vendedor con identificación ${usuario.cedula}`;
    // if(formaPago.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(usuario);
      }});
    // }
   }

   serviceCambiarEstado(usuario: Usuario){
       this._serviceVendedor.cambiarEstadoVendedor(usuario.idPersona).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getVendedores();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

   getEstado(estado: string){
    return this.listaEstados.find(x=> x.substr(0,1) === estado);
  }

}
