import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { TipoFormaPago } from '../../../models/tipoFormaPago';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ServiceTipoFormaPago } from '../../../services/tipoFormaPago.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Mensajes } from '../../../utils/mensajes';
import { HttpErrorResponse } from '@angular/common/http';
import { CreateTipoFormaPagoComponent } from '../create-tipo-forma-pago/create-tipo-forma-pago.component';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-tipo-forma-pago',
  templateUrl: './lista-tipo-forma-pago.component.html',
  styleUrls: ['./lista-tipo-forma-pago.component.scss']
})
export class ListaTipoFormaPagoComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'TFPL';
  _ACCIONES = 'CRUD';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaTipoFPagos: TipoFormaPago [] = [];
  nroFormaPagos = 0;
  listaEstados = ['ACTIVO','INACTIVO']
  isLoading = true;
  formaPagoSeleccionado: TipoFormaPago = new TipoFormaPago();
  idNegocio = Number(ConstantsService.getNegocioKey());
  //Información MatTable
  dataSource: MatTableDataSource<TipoFormaPago> = new MatTableDataSource<TipoFormaPago>(this.listaTipoFPagos);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(tipoPag: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && tipoPag !== undefined ){
      this.dataSource.paginator = tipoPag;
      tipoPag._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      tipoPag._intl.previousPageLabel = this.mensajes.itemAnterior;
      tipoPag._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',
    'nombre',
    'descripcion',
    'estado',
  ];

  constructor(private _serviceTipoFPago: ServiceTipoFormaPago, private matDialog: MatDialog, private router: Router, private mensajes: Mensajes) {
    ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
   }

   ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getFormaPagos();  
    }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

   getFormaPagos() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de formaPagos', true, false);
    }
    this._serviceTipoFPago.getTipoFPago(this.idNegocio).subscribe( (result: TipoFormaPago[]) => {
      this.listaTipoFPagos = result;
      this.asignarMatTable(this.listaTipoFPagos);
      // this.ngxDatatable.element.click();
      console.log(this.listaTipoFPagos)
      this.nroFormaPagos = this.listaTipoFPagos.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(formaPagos: TipoFormaPago[]){
    this.dataSource.data = formaPagos;
    // this.dataSource.paginator = this.paginator;
  }

  crearElementoModal(accion: string,formaPago?: TipoFormaPago){
    return {
      accion,
      formaPago: formaPago ? formaPago: null
    }
  }


  actionCreate(accion: string,formaPago?: TipoFormaPago){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, formaPago)
    }
    const dialogRef = this.matDialog.open(CreateTipoFormaPagoComponent, {
      // width: '90%',
      // height: '90%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getFormaPagos();
    });
  }

  cambiarEstado(formaPago: TipoFormaPago){
    let mensaje =`Se cambiará el estado del formaPago ${formaPago.nombre}`;
    // if(formaPago.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(formaPago);
      }});
    // }
   }

   serviceCambiarEstado(formaPago: TipoFormaPago){
       this._serviceTipoFPago.cambiarEstadoTipoFPago(formaPago.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getFormaPagos();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

  refreshPage(){
    this.router.routeReuseStrategy.shouldReuseRoute = ()=> false;
    this.router.onSameUrlNavigation ='reload';
    this.router.navigate(['/core/formaPago/lista']);
  }
}
