import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTipoFormaPagoComponent } from './lista-tipo-forma-pago.component';

describe('ListaFormaPagoComponent', () => {
  let component: ListaTipoFormaPagoComponent;
  let fixture: ComponentFixture<ListaTipoFormaPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaTipoFormaPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTipoFormaPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
