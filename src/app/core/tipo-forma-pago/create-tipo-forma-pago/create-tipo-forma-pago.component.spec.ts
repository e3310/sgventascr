import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTipoFormaPagoComponent } from './create-tipo-forma-pago.component';

describe('CreateTipoFormaPagoComponent', () => {
  let component: CreateTipoFormaPagoComponent;
  let fixture: ComponentFixture<CreateTipoFormaPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTipoFormaPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTipoFormaPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
