import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { TipoFormaPago } from '../../../models/tipoFormaPago';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { ServiceTipoFormaPago } from '../../../services/tipoFormaPago.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-tipo-forma-pago',
  templateUrl: './create-tipo-forma-pago.component.html',
  styleUrls: ['./create-tipo-forma-pago.component.scss']
})
export class CreateTipoFormaPagoComponent implements OnInit {
  
  _ABREVIATURA= 'TFPC';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = ConstantsService.getNegocioKey();
  formaPagoSelected: TipoFormaPago = new TipoFormaPago();

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;

  isLoading = true;
  
  constructor(private fb: FormBuilder, private _serviceTipoFPago: ServiceTipoFormaPago, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      this.initValidators();
      console.log(parametros)
      this.accion=parametros.accion;     
      this.formaPagoSelected = parametros.formaPago;
      
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);    
    this.initialize();    
  }

  initialize() {
    if (this.accion === 'c') {
      this.title = 'Crear nuevo tipo';
    } else {
      this.title ='Editar tipo';
      this.asignarFormaPagoForm();
    }
    SweetAlert.close();  
  }

  initValidators() {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.maxLength(45)]],
      descripcion: ['', [Validators.maxLength(150)]]
     });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarFormaPagoCrear() {   
    let formaPago: TipoFormaPago = this.form.value as unknown as TipoFormaPago;
      formaPago.neg_id =parseInt(this.negocioKey);
    console.log(formaPago);    
    return formaPago;
  }

  asignarFormaPagoForm() {
    this.form.patchValue(this.formaPagoSelected);
  }

  serviceCreateFormaPago(){
    this._serviceTipoFPago.saveTipoFPago(this.asignarFormaPagoCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar();  
    
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar formaPago');
    });
  }

  serviceUpdateFormaPago(){
    this._serviceTipoFPago.updateTipoFPago(this.asignarFormaPagoCrear(), this.formaPagoSelected.id).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar formaPago');
    });
  }

  guardar(){
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        this.serviceCreateFormaPago();
      } else {
        this.serviceUpdateFormaPago();
      }
      // this.mainForm.reset();
    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.matDialogRef.close('SAVE');
  }


}
