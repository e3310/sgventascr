import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Venta } from '../../../models/venta';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ServiceVenta } from '../../../services/venta.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { CreateVentaComponent } from '../create-venta/create-venta.component';
import { ExportXLSX } from '../../../utils/exportXLSX';
import { PdfMake } from '../../../utils/pdfMake';
import { Cliente } from '../../../models/cliente';
import { ServiceCliente } from '../../../services/cliente.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatRadioChange } from '@angular/material/radio';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { AppDateAdapter, MY_FORMATS } from '../../directives/format-datepicker';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DatePipe } from '@angular/common';
import { ServicePaqueteProducto } from '../../../services/paqueteProducto.service';
import { PaqueteProducto } from '../../../models/paqueteProducto';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-ventas',
  templateUrl: './lista-ventas.component.html',
  styleUrls: ['./lista-ventas.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class ListaVentasComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'VENL';
  _ACCIONES = 'CRDS';  
  _PERMISO = new Permiso();

  diasValidos = 3;
  negocioValueSesion = ConstantsService.getNegocioValue();
  negocioKey = Number(ConstantsService.getNegocioKey());
  listaVentas: Venta [] = [];
  listaTempVentas: Venta [] = [];
  listaCamposBusqueda =[{id: 'F', value:'Rango de fechas'},{id: 'P', value:'Paquete de Créditos'}, {id: 'C', value:'Cliente'}];
  criterioBusqueda = 'Cliente';

  nroVentas = 0;
  listaEstados = ConstantsService.listaEstados
  listaTiposCuenta = ConstantsService.listaTiposCuenta;

  //Elementos para input select CLIENTE
  listaClientes: Cliente[] = [];
  filteredClientes: Observable<Cliente[]> = new Observable();
  isValidCliente = true;
  clienteSeleccionado = new Cliente();
  listaPaquetes: PaqueteProducto[] = [];

  isLoading = true;
  ventaSeleccionada: Venta = new Venta();
  idNegocio = Number(ConstantsService.getNegocioKey());
  //Información MatTable
  dataSource: MatTableDataSource<Venta> = new MatTableDataSource<Venta>(this.listaVentas);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(venta: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && venta !== undefined ){
      this.dataSource.paginator = venta;
      venta._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      venta._intl.previousPageLabel = this.mensajes.itemAnterior;
      venta._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',
    'userCliente',
    'nombreVendedor',
    'nombrePaquete',
    // 'tfp_id',
    // 'fp_id',
    'estado',
    'cantidad',
    'bonificacion',
    'total_det',
    'fecha_registro'
  ];

  // FormBuilder de búsqueda
  searchForm: FormGroup;
  submitted = false;
  formValid = true;
  isBusquedaActiva = false;
  fechaInicioField = new FormControl(new Date(),Validators.required);
  fechaFinField = new FormControl(new Date(),Validators.required);
  clienteField = new FormControl('', [Validators.required]);
  paqueteField = new FormControl(1, [Validators.required]);
  //Alternar input
  isVisibleFecha = false;
  isVisiblePaquete = false;
  isVisibleCliente = true;
  valor1 = null;
  valor2 = null;
  campoBusqueda = 'C';


  //Checbox descarga
  isVisualizar = false;
  isDescargar = true;
  listaVentasSearch: Venta [] = [];

  currentDay : Date;
  firstDayMonth : Date;
  lastDayMonth : Date;
  minDate : Date;

  constructor(private _serviceVenta: ServiceVenta, private _serviceCliente: ServiceCliente, private _servicePaquete: ServicePaqueteProducto,
    private matDialog: MatDialog, private mensajes: Mensajes, public datepipe: DatePipe,
    private fb: FormBuilder) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
      this.currentDay = new Date();
      this.firstDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth(), 1);
      this.lastDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth() + 1, 0);
      this.minDate = new Date(2021,0,1);
   }

  ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getVentas();
      this.getClientes();
    }
  }

  ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

  getVentas() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de ventas', true, false);
    }
    this._serviceVenta.getVentas(this.idNegocio).subscribe( (result: Venta[]) => {
      this.listaVentas = result;
      this.listaTempVentas = result;
      this.asignarMatTable(this.listaVentas);
      // this.ngxDatatable.element.click();
      console.log(this.listaVentas)
      this.nroVentas = this.listaVentas.length;
      SweetAlert.close();
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(ventas: Venta[]){
    this.dataSource.data = ventas;
    // this.dataSource.paginator = this.paginator;
    // this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string,venta?: Venta){
    return {
      accion,
      venta: venta ? venta: null
    }
  }


  actionCreate(accion: string,venta?: Venta){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, venta)
    }
    console.log(accion, datos);

    const dialogRef = this.matDialog.open(CreateVentaComponent, {
      // width: '50%',
      // height: '70%',
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getVentas();
    });
  }

  cambiarEstado(venta: Venta){
    let mensaje =`Se cambiará el estado del venta V-${venta.id}`;
    // if(venta.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(venta);
      }});
    // }
   }

   serviceCambiarEstado(venta: Venta){
       this._serviceVenta.cambiarEstadoVenta(venta.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getVentas();
      }, (error: HttpErrorResponse) => {
        if(error['status']===403){
          SweetAlert.show(false, 'error', `No puede anular ventas registradas hace más de ${this.diasValidos} dias.` ,
           'Imposible cambiar estado');
        }else{
          console.log(error);
          SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
        }
      });
   }

   // BUSCAR VENTAS
   initSearchForm() {
    this.searchForm = this.fb.group({
      campoBusqueda : new FormControl('C', [Validators.required, Validators.maxLength(1)]),
      inicio: this.fechaInicioField,
      fin: this.fechaFinField,
      cliente: this.clienteField,
      paquete: this.paqueteField,
      visualizar: new FormControl(false),
      descargar: new FormControl(true)
     });
     console.log(this.searchForm);
  }

  actionViewFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
    this.isBusquedaActiva = true;
    this.initSearchForm();    
    SweetAlert.close();
  }

  actionHideFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
    this.isBusquedaActiva = false;
    this.listaVentas = [...this.listaTempVentas];
    this.dataSource.data = this.listaVentas;
    this.initSearchForm();
    SweetAlert.close();
  }

  actionRadioChange(mrChange: MatRadioChange){
    let aux= this.listaCamposBusqueda.find(x=> x.id === mrChange.value);
    if(aux !== null){
      this.criterioBusqueda = aux.value;
      this.listaVentas = [...this.listaTempVentas];
      this.dataSource.data = this.listaVentas;
      // this.searchForm.controls['valorBusqueda'].setValue('');
      this.actionSearchCriterio(aux.id);
      console.log('field selected checkbox',aux.id);
      
    }else{
      this.criterioBusqueda = 'Nombres aux';
    }
  }

  actionSearchCriterio(id: string){
    this.campoBusqueda = id;
    switch(id){
      case 'F':
        this.asignarValoresVisuales(1);
        this.asignarValoresFields(this.paqueteField, this.clienteField, true);
        break;
      case 'P':
        this.asignarValoresVisuales(2);
        this.restartFieldsFechas();
        this.asignarValoresFields(this.paqueteField, this.clienteField, false);
        break;
      case 'C':
        this.asignarValoresVisuales(3);
        this.restartFieldsFechas();
        this.asignarValoresFields(this.clienteField, this.paqueteField, false);        
        break;
    }
  }

  asignarValoresVisuales(posicionVerdadero: number){
    // 'use strict';
    this.isVisibleFecha = false, this.isVisiblePaquete = false, this.isVisibleCliente = false;
    switch(posicionVerdadero){
      case 1:
        this.isVisibleFecha = true;
        break;
      case 2:
        this.isVisiblePaquete = true;
        break;
      case 3:
        this.isVisibleCliente = true;
        break;
    }
  }

  restartFieldsFechas(){
    this.fechaInicioField.setValue(new Date());
    this.fechaFinField.setValue(new Date());
  }

  asignarValoresFields(fieldVacio: FormControl, fieldAsignado: FormControl, isOtherField = true){
    if(isOtherField === true){ //Si el campo seleccionado es diferente a los campos ingresados como parametros
      fieldVacio.setValue(0);
    }else{
      fieldVacio.setValue('');
    }
    fieldAsignado.setValue(0);
  }

  asignarValoresporCheckBox(field: string, valor1: any, valor2 = 'vacio'){
    this.valor1 = valor1;
    this.valor2 = valor2;
    this.campoBusqueda = field;
  }

  asignarValoresRequest(){
    this.limpiarFieldsRequest();
    switch(this.campoBusqueda){
      case 'F':
        let inicio = this.searchForm.get('inicio').value? this.searchForm.get('inicio').value: Date.now();
        let fin = this.searchForm.get('fin').value? this.searchForm.get('fin').value: Date.now();
        this.asignarValoresporCheckBox(this.campoBusqueda, this.datepipe.transform(inicio, 'yyyy-MM-dd'), this.datepipe.transform(fin, 'yyyy-MM-dd'));
        break;
      case 'P':
        this.asignarValoresporCheckBox(this.campoBusqueda,this.paqueteField.value);
        break;
      case 'C':
        this.asignarValoresporCheckBox(this.campoBusqueda,this.clienteField.value.id);
        break;
    }
    console.log(this.searchForm);
    
  }

  limpiarFieldsRequest(){
    this.valor1 = null;
    this.valor2 = null;
  }

  actionSearch(tipo: string){
    this.submitted = true;
    this.searchForm.markAllAsTouched();
    this.listaVentasSearch = [];
    if(this.searchForm.valid){
      this.formValid = true;
      this.isLoading = true;
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
      this.serviceSearch(tipo);
      this.isLoading = false;
    }else{
      console.log(this.searchForm);
      this.formValid = false;
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.searchForm.controls[controlName].hasError(errorName);
  }

  actionChange(checked: boolean, accion: string){
    if(accion === 'v' && checked){
      this.searchForm.controls['descargar'].setValue(false);
      this.isVisualizar = true;
      this.isDescargar = false;
    }else if(accion === 'd' && checked){
      this.searchForm.controls['visualizar'].setValue(false);
      this.isVisualizar = false;
      this.isDescargar = true;
    }
  }

  actionSearchVisualizar(result: Venta[]){
    console.log('VER',result);
    this.listaVentas =[...result];
    this.dataSource.data = result;
  }

  actionSearchDescargar(result: Venta[], tipo: string){
    let reporte = [];
      result.forEach((e: Venta) => {
        let item = [ e.userCliente, e.nombreVendedor, e.nombrePaquete, 
        e.cantidad, e.bonificacion, e.total_det];
          reporte.push(item);
      });
      if  (reporte.length > 0){
        let encabezado = ['CLIENTE', 'VENDEDOR', 'PAQUETE', 
        'CANTIDAD', 'CANTIDAD PROMOCIONAL', 'TOTAL'];
        switch(tipo){
          case 'p':
            this.descargarReportePdf(encabezado,reporte);
            break;
          case 'e':
            this.descargarReporteExcel(encabezado,reporte);
            break;
        }
        SweetAlert.close();
      }else{
        SweetAlert.show(false, 'warning', 'No se encontraron resultados para la búsqueda', 'No se ha generado el archivo', false, true);
      }

  }

  descargarReporteExcel(encabezado: any,lista: any[]){
    ExportXLSX.downloadXLSX(encabezado, lista, 'Ventas_por_fecha');
    // SweetAlert.close();
  }

  descargarReportePdf(encabezado: any,lista: any[]) {
    var pdfGenerator =
    PdfMake.createPdf(encabezado, lista, 'Ventas por fecha', 'Administrador', 'landscape');
    PdfMake.downloadPdf(pdfGenerator, 'Ventas_por_fecha');
  }

  serviceSearch(tipo: string){
    this.asignarValoresRequest();
    this._serviceVenta.searchVentas(this.idNegocio,this.valor1,this.valor2, this.campoBusqueda).subscribe( (result: Venta[]) => {
      if(this.searchForm.controls['visualizar'].value === true){
      this.actionSearchVisualizar(result);
      }else{
        this.actionSearchDescargar(result, tipo);
      }
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  /**
   * SELECT INPUT CLIENTE
   */

   getClientes() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    }
    this._serviceCliente.getClientesActivos(this.negocioKey).subscribe( (result: Cliente[]) => {
      this.listaClientes = result;
      this.addFilterCliente();
      this.getPaquetes();
      // SweetAlert.close();
      // this.initialize();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de clientes');
    });
  }

  getPaquetes(){
    // if (this.isLoading) {
    //   SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de paquetes', true, false);
    // }
    this._servicePaquete.getPaquetesActivos(this.negocioKey).subscribe( (result: PaqueteProducto[]) => {
      this.listaPaquetes = result;
      this.isLoading = false;
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de paquetes');
    });
  }

  displayCliente(cliente: Cliente): string {
    this.clienteSeleccionado = cliente;
    console.log(cliente);
    return cliente ? `${cliente.usuario_credito}` : '';
  }

  private _filterCliente(value: string): Cliente[] {
    // console.log('VALUE TO FILTER', value);
    // this.listaClienteesEncontrados = this.listaClientees.filter(cliente => cliente.nombre.includes(value))
    if(value.length >= 1){
    return this.listaClientes.filter(cliente => cliente.usuario_credito.toUpperCase().includes(value.toUpperCase()));
    }else {
      return this.listaClientes;
    }
  }

  addFilterCliente(): void {
    this.filteredClientes = this.clienteField.valueChanges.pipe(
      startWith(''),
      map((value) => {
        console.log('VALUE CHANGES CLIENTE',value);
        const val = value.usuario_credito? value.usuario_credito : value;
        const result = this._filterCliente(val);
        this.isValidCliente = result.length > 0;
        return result;
      }));
  }

  blurCliente(): void {
    if (!this.isValidCliente) {
      this.clienteField.setValue('');
    }
  }

  closedCliente(): void {
    let id = this.clienteField.value.id;
    if(id){

    }else{
      this.clienteField.setValue('');
    }
    // const find = this.listaClienteesEncontrados.find(element => element.id === id);

  }

}
