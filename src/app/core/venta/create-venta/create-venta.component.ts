import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Venta, VentaDetalle } from '../../../models/venta';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ServiceVenta } from '../../../services/venta.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { Cliente } from '../../../models/cliente';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ServiceCliente } from '../../../services/cliente.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ServiceVendedor } from '../../../services/vendedor.service';
import { Vendedor } from '../../../models/vendedor';
import { ServiceTipoFormaPago } from '../../../services/tipoFormaPago.service';
import { ServiceFormaPago } from '../../../services/formaPago.service';
import { ServicePaqueteProducto } from '../../../services/paqueteProducto.service';
import { TipoFormaPago } from '../../../models/tipoFormaPago';
import { FormaPago } from '../../../models/formaPago';
import { PaqueteProducto } from '../../../models/paqueteProducto';

@Component({
  selector: 'app-create-venta',
  templateUrl: './create-venta.component.html',
  styleUrls: ['./create-venta.component.scss']
})
export class CreateVentaComponent implements OnInit {

  _ABREVIATURA= 'VENC';
  isUserVendedor = false;
  nombresUserVendedor: string;
  idUserVendedor : number;
  
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = Number(ConstantsService.getNegocioKey());
  ventaSelected: Venta = new Venta();
  stringCuentaBancaria = ConstantsService.stringCuentaBancaria;
  isCheckedPromocion = false;
  isShowAction = false;

  // FormBuilder
  // form: FormGroup;
  submitted = false;
  formValid = true;

  isLoading = true;
  isCuentaBancaria = false;

  listaVentas : Venta[] = [];

  //Elementos para input select CLIENTE
  listaClientesEncontrados: Cliente[] = [];
  filteredClientes: Observable<Cliente[]> = new Observable();
  isValidCliente = true;  
  clienteSeleccionado = new Cliente();

  //Elementos para input select VENDEDOR
  listaVendedoresEncontrados: Vendedor[] = [];
  filteredVendedores: Observable<Vendedor[]> = new Observable();
  isValidVendedor = true;  
  vendedorSeleccionado = new Vendedor();

  //Elementos para input select TIPO FORMA PAGO
  listaTipoFPagoEncontrados: TipoFormaPago[] = [];

  //Elementos para input select FORMA PAGO
  listaFormaPagoEncontrados: FormaPago[] = [];

  //Elementos para input select PAQUETE
  listaPaquetesEncontrados: PaqueteProducto[] = [];
  paqueteSeleccionado = new PaqueteProducto();

  clienteField: FormControl = new FormControl('', [Validators.required]);
  vendedorField: FormControl = new FormControl('', [Validators.required]);

  form: FormGroup = new FormGroup({
    cli_id: this.clienteField,
    vend_id: this.vendedorField,
    tfp_id: new FormControl('', [Validators.required]),
    fp_id: new FormControl('', [Validators.required]),
    paq_id: new FormControl('', [Validators.required]),
    cantidad: new FormControl(1, [Validators.required, Validators.min(1)]),
    bonificacion: new FormControl(0, [Validators.required, Validators.min(0)]),
    total_det: new FormControl(0, [Validators.required, Validators.min(1)]),
    //no insertable
    valorUnitario: new FormControl(0,[Validators.required, Validators.min(0.1)])
  });
    
  constructor(private _serviceVenta: ServiceVenta, private _serviceCliente: ServiceCliente,
    private _serviceVendedor: ServiceVendedor, private _serviceTipoFPago: ServiceTipoFormaPago,
    private _serviceFormaPago: ServiceFormaPago, private _servicePaquete: ServicePaqueteProducto,
     private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) {  
      if(ConstantsService.getUserType() === ConstantsService.typeUserVendor){
        this.isUserVendedor = true;
      }
      console.log('INPUT PARAM',parametros);
      this.accion=parametros.accion;     
       
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);  
    this.getClientes();  
    this.initialize();   
    
  }

  initialize() {    
    if (this.accion === 'c') {
      this.title = 'Registrar venta';
    } else { // accion s => show
      this.isShowAction = true;
      this.ventaSelected = this.parametros.kardex;   
      this.title = `Venta de origen V-${this.ventaSelected.id}`;
      this.asignarVentaForm();
      this.getFormaPago(this.ventaSelected.tfp_id);
      if(this.ventaSelected.bonificacion>0){
        this.isCheckedPromocion = true
        this.isVisiblePromocion = true;
      }
      let valUnitarioInicial = this.ventaSelected.total_det/ this.ventaSelected.cantidad;
      this.form.controls['valorUnitario'].setValue(valUnitarioInicial);
      this.form.disable();
    }
    // SweetAlert.close();  
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarVentaCrear() {   
    let venta: Venta = this.form.value as unknown as Venta;
    // let auxSaldo = cuentaBancaria.saldo? cuentaBancaria.saldo: 0;
    // cuentaBancaria.saldo = auxSaldo;
    venta.neg_id =this.negocioKey;
    venta.usuario_registro = Number(ConstantsService.getUserKey());
    let cantidad = Number(this.form.controls['cantidad'].value);
    let bonificacion = Number(this.form.controls['bonificacion'].value);
    let totalDet = Number(this.form.controls['total_det'].value);
    venta.cantidadVenta = (cantidad * this.paqueteSeleccionado.nro_unidades) + bonificacion;
    venta.kar_id =0;
    venta.cli_id = this.form.controls['cli_id'].value['id'];
    venta.detalles = [];
    venta.detalles.push(this.crearDetalleVenta(this.paqueteSeleccionado.id, cantidad, bonificacion,totalDet));   
    return venta;
  }

  crearDetalleVenta(idPaquete: number, cantidad: number, bonificacion:number, totalDet: number){
    let det : VentaDetalle  = {
      cantidad : cantidad,
      paq_id : idPaquete,
      total_det: totalDet,
      bonificacion: bonificacion
    }
    return det;
  }

  serviceCreateVenta(){
    this._serviceVenta.saveVenta(this.asignarVentaCrear()).subscribe(result =>{
      this.accionesGuardar();  
    
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar venta');
    });
  }

  guardar(){
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      SweetAlert.show(false, 'info', 'Procesando información', '', true); 
      this.formValid = true;
      if (this.accion === 'c') {
        this.serviceCreateVenta();
      } 

    }else{
      this.formValid = false;
    }
  }

  asignarVentaForm() {
    this.form.patchValue(this.ventaSelected);
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, true);
    this.matDialogRef.close('SAVE');
  }


  /**
   * SELECT INPUT CLIENTE
   */

   getClientes() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información', true, false);
    }
    this._serviceCliente.getClientesActivos(this.negocioKey).subscribe( (result: Cliente[]) => {
      this.listaClientesEncontrados = result;
      this.addFilterCliente();           
      this.cargarInfoVendedores();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de clientes');
    });
  }

  displayCliente(cliente: Cliente): string {
    this.clienteSeleccionado = cliente;
    console.log(cliente);
    return cliente ? `${cliente.usuario_credito}` : '';
  }

  private _filterCliente(value: string): Cliente[] {
    // this.listaClienteesEncontrados = this.listaClientees.filter(cliente => cliente.nombre.includes(value))
    // if(value.length >= 2){
    return this.listaClientesEncontrados.filter(cliente => cliente.usuario_credito.toUpperCase().includes(value.toUpperCase()));
    // }else {
      // return this.listaClienteesEncontrados;
    // }
  }

  addFilterCliente(): void {
    this.filteredClientes = this.clienteField.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const val = value.usuario_credito? value.usuario_credito : value;
        const result = this._filterCliente(val);
        this.isValidCliente = result.length > 0;
        return result;
      }));
  }

  blurCliente(): void {
    if (!this.isValidCliente) {
      this.clienteField.setValue('');
    }
  }

  closedCliente(): void {
    let id = this.clienteField.value.id;
    if(id){

    }else{
      this.clienteField.setValue('');
    }
    // const find = this.listaClienteesEncontrados.find(element => element.id === id);
    
  }

  showInfoSelectedClient(){
    if(this.clienteSeleccionado){
      if(this.clienteSeleccionado.nombres && this.clienteSeleccionado.apellidos){
        return `${this.clienteSeleccionado.nombres} ${this.clienteSeleccionado.apellidos}`;
      }else return '';
    }
  }
  
  cargarClienteSeleccionado(idCliente: number){
    let cliente = this.listaClientesEncontrados.find(x=> x.id === idCliente);
    return cliente;
  }

  /**
 * SELECT INPUT VENDEDOR
 */

  getVendedores() {
    // if (this.isLoading) {
    //   SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de vendedores', true, false);
    // }
    this._serviceVendedor.getVendedoresActivos(this.negocioKey).subscribe( (result: Vendedor[]) => {
      this.listaVendedoresEncontrados = result;        
      this.getTipoFormaPago();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de vendedores');
    });
  }

/**
 * SELECT INPUT TIPOS DE FORMAS DE PAGO
 */

  getTipoFormaPago() {
    // if (this.isLoading) {
    //   // SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de formas de pago', true, false);
    // }
    this._serviceTipoFPago.getTipoFPagoActivos(this.negocioKey).subscribe( (result: TipoFormaPago[]) => {      
      this.listaTipoFPagoEncontrados = result;          
      this.getPaquetes();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de formas de pago');
    });
  }

  getFormaPago(idTipo: number){
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de cuentas', true, false);
    }
    this._serviceFormaPago.getFormasPagoActivos(this.negocioKey, idTipo).subscribe( (result: FormaPago[]) => {      
      this.listaFormaPagoEncontrados = result;          
      this.isLoading = false;
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de cuentas');
    });
  }

  getPaquetes(){
    // if (this.isLoading) {
    //   SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando información de paquetes', true, false);
    // }
    this._servicePaquete.getPaquetesActivos(this.negocioKey).subscribe( (result: PaqueteProducto[]) => {           
      this.listaPaquetesEncontrados = result;          
      this.isLoading = false;
      SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información de paquetes');
    });
  }
  
  getCalculo(){    
    let unitario=0;
    let total =0;    
    if(this.form.controls['paq_id'].value && this.form.controls['paq_id'].value !== ''){
      let idPaquete = this.form.controls['paq_id'].value;
      let cantidad = this.form.controls['cantidad'].value;
      this.paqueteSeleccionado = this.listaPaquetesEncontrados.find(x=> x.id === idPaquete);
      unitario = this.paqueteSeleccionado.precio_venta;
      total = cantidad* unitario;      
    }
    this.form.controls['valorUnitario'].setValue(unitario);
    this.form.controls['total_det'].setValue(total);
  }

  isVisiblePromocion = false;

  verPromocion(valor: boolean) {
    this.isCheckedPromocion = valor;
    this.isVisiblePromocion = valor;
  }

  serviceVendedorPorUser(){
    // "nombres": "LAURA",
    // "apellidos": "FARINANGO",
    // "idVendor": 20
    this._serviceVendedor.getVendedoresPorUser(Number(ConstantsService.getUserKey())).subscribe(result => {           
      this.nombresUserVendedor = `${result["nombres"]} ${result["apellidos"]}`;
      this.idUserVendedor =result["idVendor"];  
      this.vendedorField.setValue(this.idUserVendedor);
      this.getTipoFormaPago();
      // SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar vendedor');
    });
  }

  cargarInfoVendedores(){
    if(this.isUserVendedor === true){
      this.serviceVendedorPorUser();
    }else{
      this.getVendedores();
    }
  }
}
