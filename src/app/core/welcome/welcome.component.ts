import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConstantsService } from '../../services/constants.service';
import { Negocio } from '../../models/negocio';
import { ServiceNegocio } from '../../services/negocio.service';
import { SweetAlert } from 'app/utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SeleccionarNegocioComponent } from '../negocio/seleccionar-negocio/seleccionar-negocio.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  _ABREVIATURA= 'WELC'

  @ViewChild('content') templateRef;

  activemodal: NgbActiveModal;
  listaNegocios: Negocio [] = [];
  listaEstados = ['ACTIVO','INACTIVO'];
  isLoading = true;
  listaVacia = false;
  usuarioNombreLogueado = sessionStorage.getItem('userName');
  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();

  constructor(private _serviceNegocio: ServiceNegocio, private router: Router, private matDialog: MatDialog) {
    // this.openNegocios(this.content)

  }

  ngOnInit(){
    console.log(ConstantsService.getNegocioKey());    
    if(ConstantsService.getNegocioKey() === null){
      this.actionSeleccionarNegocio();
    }
  }

  actionSeleccionarNegocio(){
    const dialogRef = this.matDialog.open(SeleccionarNegocioComponent, {     
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(data => {
      setTimeout(() => {
        if(data === 'OK'){
          this.refreshPage();
        }
      }, 3000);      
    });
  }

  // getNegocios() {
  //   if (this.isLoading) {
  //     SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de negocios', true, false);
  //   }
  //   this._serviceNegocio.getNegocios().subscribe( (result: Negocio[]) => {
  //     this.listaNegocios = result;
  //     console.log(this.listaNegocios)
  //     SweetAlert.close();      
  //     if(this.listaNegocios.length <= 0){
  //       this.listaVacia = true;
  //     }
  //     this.activemodal = this.modalService.open(this.templateRef, { scrollable: true, centered: true, backdrop: 'static' });
  //     this.isLoading = false;
  //   }, (error: HttpErrorResponse) => {
  //     console.log(error);
  //     this.listaVacia = true;
  //     SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
  //   });
  // }

  // accionSelectNegocio(negocioSeleccionado: Negocio){
  //   if(negocioSeleccionado.estado === 'A'){
  //     this.accionChangeValues(negocioSeleccionado);         
  //   }else{
  //     SweetAlert.show(false,'error', 'No puede seleccionar este negocio', 'Negocio Inactivo', false, true);
  //   }
  // }

  // accionChangeValues(negocioSeleccionado: Negocio){
  //   ConstantsService.idNegocio = negocioSeleccionado.id;     
  //   ConstantsService.nombreNegocio = negocioSeleccionado.nombre; 
  //   ConstantsService.saveSessionNegocio();   
  //   this.modalService.dismissAll();
  //   this.refreshPage();
  //   SweetAlert.show(false, 'success', `Toda la información en el sistema se visualizará en base a la información del negocio ${negocioSeleccionado.nombre.toUpperCase()}`,
  //   `Ha seleccionado ${negocioSeleccionado.nombre.toUpperCase()}`,false, true)
  // }

  // imprimir(){
  //     console.log(ConstantsService.idNegocio)
  // }

  refreshPage(){    
    this.router.routeReuseStrategy.shouldReuseRoute = ()=> false;
    this.router.onSameUrlNavigation ='reload';
    this.router.navigate(['/core/welcome']);   
    window.location.reload(); 
  }

}
