import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFormaPagoComponent } from './create-forma-pago.component';

describe('CreateFormaPagoComponent', () => {
  let component: CreateFormaPagoComponent;
  let fixture: ComponentFixture<CreateFormaPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFormaPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFormaPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
