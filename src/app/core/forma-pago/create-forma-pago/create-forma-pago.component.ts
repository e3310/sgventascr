import { Component, OnInit, Inject } from '@angular/core';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ServiceFormaPago } from '../../../services/formaPago.service';
import { ConstantsService } from '../../../services/constants.service';
import { FormaPago } from '../../../models/formaPago';
import { ServiceTipoFormaPago } from '../../../services/tipoFormaPago.service';
import { TipoFormaPago } from '../../../models/tipoFormaPago';

@Component({
  selector: 'app-create-forma-pago',
  templateUrl: './create-forma-pago.component.html',
  styleUrls: ['./create-forma-pago.component.scss']
})
export class CreateFormaPagoComponent implements OnInit {
  
  _ABREVIATURA= 'FPC';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = Number(ConstantsService.getNegocioKey());
  formaPagoSelected: FormaPago = new FormaPago();
  stringCuentaBancaria = ConstantsService.stringCuentaBancaria;

  // FormBuilder
  // form: FormGroup;
  submitted = false;
  formValid = true;

  isLoading = true;
  isCuentaBancaria = false;

  listaTiposCuenta = ConstantsService.listaTiposCuenta;
  listaTipoFPago : TipoFormaPago[] = [];
  tipoCuentaField: FormControl = new FormControl(null);
  tipoFPagoField: FormControl = new FormControl('', [Validators.required]);

  form: FormGroup = new FormGroup({
    numero: new FormControl('', [Validators.required, Validators.maxLength(40)]),
    tipo_cuenta: this.tipoCuentaField,
    saldo: new FormControl(0, [Validators.min(0)]),
    entidad_bancaria: new FormControl('', [Validators.required, Validators.maxLength(200)]),
    nombres_titular: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    apellidos_titular: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    ci_titular: new FormControl('', [Validators.required, Validators.maxLength(15)]),
    telefono_titular: new FormControl('', [Validators.maxLength(15)]),
    email_titular: new FormControl('', [Validators.maxLength(45)]),
    tipofp : this.tipoFPagoField
  });
  
  constructor(private _serviceFormaPago: ServiceFormaPago, private _serviceTipoFPago: ServiceTipoFormaPago,
     private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      // this.initValidators();
      console.log(parametros)
      this.accion=parametros.accion;     
      this.formaPagoSelected = parametros.formaPago;
      
    }

  ngOnInit(): void {
    this.getTipoFormasPago();
    SweetAlert.show(false, 'info', 'Cargando información', '', true);    
    this.initialize();    
  }

  initialize() {    
    if (this.accion === 'c') {
      this.title = 'Crear nueva cuenta';
    } else {
      this.title ='Editar cuenta';
      this.asignarCuentaBancariaForm();
    }
    SweetAlert.close();  
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarCuentaBancariaCrear() {   
    let cuentaBancaria: FormaPago = this.form.value as unknown as FormaPago;
    let auxSaldo = cuentaBancaria.saldo? cuentaBancaria.saldo: 0;
    cuentaBancaria.saldo = auxSaldo;
    cuentaBancaria.neg_id =this.negocioKey;
    cuentaBancaria.usuario_registro = Number(ConstantsService.getUserKey());
    console.log(cuentaBancaria);    
    return cuentaBancaria;
  }

  asignarCuentaBancariaForm() {
    this.form.patchValue(this.formaPagoSelected);    
    this.tipoFPagoField.setValue(this.formaPagoSelected.tipofp);    
  }

  getTipoFormasPago(){
    this._serviceTipoFPago.getTipoFPagoActivos(this.negocioKey).subscribe(result =>{
      this.listaTipoFPago = result;
      let id = this.formaPagoSelected? this.formaPagoSelected.tipofp: 0;
      this.isCuentaBancaria = this.buscarCuentaBancaria(id);            
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar cuentaBancaria');
    });
  }

  serviceCreateCuentaBancaria(){
    this._serviceFormaPago.saveFormaPago(this.asignarCuentaBancariaCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar();  
    
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar cuentaBancaria');
    });
  }

  serviceUpdateCuentaBancaria(){
    this._serviceFormaPago.updateFormaPago(this.asignarCuentaBancariaCrear(), this.formaPagoSelected.id).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar cuentaBancaria');
    });
  }

  guardar(){
    console.log('form', this.form);
    
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        this.serviceCreateCuentaBancaria();
      } else {
        this.serviceUpdateCuentaBancaria();
      }
      // this.mainForm.reset();
    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.matDialogRef.close('SAVE');
  }

  showSelectTipoCuenta(){
    if(this.listaTipoFPago.length >0){
      let id= this.tipoFPagoField.value? this.tipoFPagoField.value : 0;
      let buscarCB = this.buscarCuentaBancaria(id);
      this.isCuentaBancaria = buscarCB;      
      if(buscarCB === false){
        this.tipoCuentaField.setValue(null);
      }
    }
  }

  buscarCuentaBancaria(idTipoFPago: number){
    let pagoNombre = this.listaTipoFPago.find(x => x.id === idTipoFPago);  
    console.log('lista pagos',this.listaTipoFPago);
    console.log('pago nombre',pagoNombre);
    
    if(pagoNombre){
      if( pagoNombre.nombre === this.stringCuentaBancaria){
        console.log('string cb', this.stringCuentaBancaria);
        
        return true;
      }
    }
    return false;
  }
}
