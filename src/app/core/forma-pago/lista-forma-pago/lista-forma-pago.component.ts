import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConstantsService } from '../../../services/constants.service';
import { FormaPago } from '../../../models/formaPago';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { CreateFormaPagoComponent } from '../create-forma-pago/create-forma-pago.component';
import { ServiceFormaPago } from '../../../services/formaPago.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-forma-pago',
  templateUrl: './lista-forma-pago.component.html',
  styleUrls: ['./lista-forma-pago.component.scss']
})
export class ListaFormaPagoComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'FPL';
  _ACCIONES = 'CRUD';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaCuentasBancarias: FormaPago [] = [];
  nroCuentasBancarias = 0;
  listaEstados = ConstantsService.listaEstados
  listaTiposCuenta = ConstantsService.listaTiposCuenta;
  isLoading = true;
  formaPagoSeleccionada: FormaPago = new FormaPago();
  idNegocio = Number(ConstantsService.getNegocioKey());
  //Información MatTable
  dataSource: MatTableDataSource<FormaPago> = new MatTableDataSource<FormaPago>(this.listaCuentasBancarias);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(formaPag: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && formaPag !== undefined ){
      this.dataSource.paginator = formaPag;
      formaPag._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      formaPag._intl.previousPageLabel = this.mensajes.itemAnterior;
      formaPag._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',
    'numero',
    'tipo_cuenta',
    'entidad_bancaria',
    'estado',
    'nombres_titular',
    'apellidos_titular',
    'ci_titular'
  ];

  constructor(private _serviceFormaPago: ServiceFormaPago, private matDialog: MatDialog, private mensajes: Mensajes) {
    ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
   }

   ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getCuentaBancarias();  
    }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

   getCuentaBancarias() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de cuentaBancarias', true, false);
    }
    this._serviceFormaPago.getFormasPago(this.idNegocio).subscribe( (result: FormaPago[]) => {
      this.listaCuentasBancarias = result;
      this.asignarMatTable(this.listaCuentasBancarias);
      // this.ngxDatatable.element.click();
      console.log(this.listaCuentasBancarias)
      this.nroCuentasBancarias = this.listaCuentasBancarias.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(cuentaBancarias: FormaPago[]){
    this.dataSource.data = cuentaBancarias;
    // this.dataSource.paginator = this.paginator;
    // this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string,formaPago?: FormaPago){
    return {
      accion,
      formaPago: formaPago ? formaPago: null
    }
  }


  actionCreate(accion: string,formaPago?: FormaPago){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, formaPago)
    }
    console.log(accion, datos);
    
    const dialogRef = this.matDialog.open(CreateFormaPagoComponent, {
      width: '50%',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getCuentaBancarias();
    });
  }

  cambiarEstado(formaPago: FormaPago){
    let mensaje =`Se cambiará el estado del formaPago ${formaPago.numero}`;
    // if(formaPago.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(formaPago);
      }});
    // }
   }

   serviceCambiarEstado(formaPago: FormaPago){
       this._serviceFormaPago.cambiarEstadoFormaPago(formaPago.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getCuentaBancarias();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }
}
