import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { GridsComponent } from "./grids/grids.component";
// import { TypographyComponent } from "./typography/typography.component";
// import { HelperClassesComponent } from "./helper-classes/helper-classes.component";
// import { SyntaxHighlighterComponent } from "./syntax-highlighter/syntax-highlighter.component";
// import { TextUtilitiesComponent } from "./text-utilities/text-utilities.component";
// import { FeatherComponent } from './icons/feather/feather.component';
// import { FontAwesomeComponent } from './icons/font-awesome/font-awesome.component';
// import { SimpleLineComponent } from './icons/simple-line/simple-line.component';
// import { ColorPaletteComponent } from './color-palette/color-palette.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ListaNegociosComponent } from './negocio/lista-negocios/lista-negocios.component';
import { ListaPaisesComponent } from './pais/lista-paises/lista-paises.component';
import { ListaClientesComponent } from './cliente/lista-clientes/lista-clientes.component';
// import { CreateClienteComponent } from './cliente/create-cliente/create-cliente.component';
import { ListaPaquetesComponent } from './producto/lista-paquetes/lista-paquetes.component';
import { ListaTipoFormaPagoComponent } from './tipo-forma-pago/lista-tipo-forma-pago/lista-tipo-forma-pago.component';
import { ListaFormaPagoComponent } from './forma-pago/lista-forma-pago/lista-forma-pago.component';
import { ListaUsuariosComponent } from './usuario/lista-usuarios/lista-usuarios.component';
import { ListaVendedoresComponent } from './vendedor/lista-vendedores/lista-vendedores.component';
import { ListaComprasComponent } from './compra/lista-compras/lista-compras.component';
import { ListaVentasComponent } from './venta/lista-ventas/lista-ventas.component';
import { ListaKardexComponent } from './kardex/lista-kardex/lista-kardex.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'welcome',
        pathMatch: 'full',
        component: WelcomeComponent,
      },
      {
        path: 'negocio/lista',
        pathMatch: 'full',
        component: ListaNegociosComponent,
      },
      {
        path: 'pais/lista',
        pathMatch: 'full',
        component: ListaPaisesComponent,
      },
      {
        path: 'cliente/lista',
        pathMatch: 'full',
        component: ListaClientesComponent,
      },
      {
        path: 'producto/lista',
        pathMatch: 'full',
        component: ListaPaquetesComponent,
      },
      {
        path: 'tipoFPago/lista',
        pathMatch: 'full',
        component: ListaTipoFormaPagoComponent,
      },
      {
        path: 'formaPago/lista',
        pathMatch: 'full',
        component: ListaFormaPagoComponent,
      },
      {
        path: 'usuario/lista',
        pathMatch: 'full',
        component: ListaUsuariosComponent,
      },
      {
        path: 'vendedor/lista',
        pathMatch: 'full',
        component: ListaVendedoresComponent,
      },    
      {
        path: 'compra/lista',
        pathMatch: 'full',
        component: ListaComprasComponent,
      },  
      {
        path: 'venta/lista',
        pathMatch: 'full',
        component: ListaVentasComponent,
      },
      {
        path: 'kardex/lista',
        pathMatch: 'full',
        component: ListaKardexComponent,
      },      
      {
        path: '**',        
        redirectTo: 'pages/error'
      }
      // {
      //   path: 'typography',
      //   component: TypographyComponent,
      //   data: {
      //     title: 'Typography'
      //   }
      // },
      // {
      //   path: 'textutilities',
      //   component: TextUtilitiesComponent,
      //   data: {
      //     title: 'Text Utilities'
      //   }
      // },
      // {
      //   path: 'syntaxhighlighter',
      //   component: SyntaxHighlighterComponent,
      //   data: {
      //     title: 'Syntax Highlighter'
      //   }
      // },
      // {
      //   path: 'helperclasses',
      //   component: HelperClassesComponent,
      //   data: {
      //     title: 'Helper Classes'
      //   }
      // },
      // {
      //   path: 'colorpalettes',
      //   component: ColorPaletteComponent,
      //   data: {
      //     title: 'Color Palette'
      //   }
      // },
      // {
      //   path: 'feather',
      //   component: FeatherComponent,
      //   data: {
      //     title: 'Feather Icons'
      //   }
      // },
      // {
      //   path: 'font-awesome',
      //   component: FontAwesomeComponent,
      //   data: {
      //     title: 'Font Awesome'
      //   }
      // },
      // {
      //   path: 'simple-line',
      //   component: SimpleLineComponent,
      //   data: {
      //     title: 'Simple Line'
      //   }
      // },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule { }
