import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetKeysUsuarioComponent } from './get-keys-usuario.component';

describe('GetKeysUsuarioComponent', () => {
  let component: GetKeysUsuarioComponent;
  let fixture: ComponentFixture<GetKeysUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetKeysUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetKeysUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
