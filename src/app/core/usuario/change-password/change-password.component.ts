import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceUsuario } from '../../../services/usuario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { CambioPassword } from 'app/models/cambioPassword';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  accion: string;
  userKey: number;
  userName: string;

  title: string;
  isActivar = false;
  subtextoActivar: string;
  model: CambioPassword = new CambioPassword();

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;
  hidePassOld = true;
  hidePassNew = true;
  hidePassNewConfirm = true;

  constructor(private fb: FormBuilder, private _serviceUsuario: ServiceUsuario, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.accion=parametros.accion;
      this.userKey = parametros.userKey;
      this.userName = parametros.userName;
    }

    ngOnInit(): void {
      SweetAlert.show(false, 'info', 'Cargando información', '', true);
      this.initValidators();
      this.initialize();
    }
  
    initialize() {
      if (this.accion === 'activar') {
        this.title = 'Su usuario requiere activacion';
        this.isActivar = true;
        this.subtextoActivar = "Por favor ingrese una nueva contraseña para activar su usuario."
        // this.form.get('nombre').setValue('');      
      } else if (this.accion === 'cambiar') {
        this.title ='Cambiar contraseña';
        // this.form.get('nombre').setValue(this.paisSelected.nombre);
      }
      SweetAlert.close();
    }
  
    initValidators() {
      this.form = this.fb.group({
        userId : [this.userKey, [Validators.required, Validators.maxLength(100)]],
        userName : [this.userName, [Validators.required, Validators.maxLength(45)]],
        passOld : ['', [Validators.required, Validators.minLength(8),Validators.maxLength(16)]],
        passNew : ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16)]],
        passNewConfirm : ['', [Validators.required, Validators.minLength(8), Validators.maxLength(16)]]
       });
    }
  
    public hasError = (controlName: string, errorName: string) => {
      return this.form.controls[controlName].hasError(errorName);
    }

    asignarModelo(){
      let modelo: CambioPassword = this.form.value as unknown as CambioPassword;
      return modelo;
    }

    serviceActivar(){
      SweetAlert.show(false,'info','Espere un momento','Validando información.',true, false);
      this._serviceUsuario.activarPendiente(this.asignarModelo()).subscribe(result =>{
        console.log(result);
        this.accionesGuardar('Usuario Activado.');        
      }, error => {
        if(error['status']===201){
          this.accionesGuardar('Usuario Activado.');
        }else{
          console.log(error);
          this.salir('ERROR');
          SweetAlert.show(false, 'error', error['error']['error'], 'Error al activar usuario');
        }
      });
    }

    serviceChangePassword(){
      SweetAlert.show(false,'info','Espere un momento','Validando información.',true, false);
      this._serviceUsuario.cambiarPassword(this.asignarModelo()).subscribe(result =>{
        console.log(result);
        this.accionesGuardar('Contraseña Actualizada.');        
      }, (error: HttpErrorResponse) => {
        if(error['status']===201){
          this.accionesGuardar('Usuario Activado.');
        }else{
          if(this.showErroresValidacion(error) === false){      
            this.salir('ERROR');
          SweetAlert.show(false, 'error', error['error']['error'], 'Error al activar usuario');
          }
        }
      });
    }

    getValidationMessages(error: HttpErrorResponse){
      let mensajes = [];
      if(error['status']===500){
        if(error['error']['message'] && error['error']['message'] === 'Validation Error.'){
          let keysValidations= Object.keys(error['error']['error']);
          keysValidations.forEach(element => {
            let validations = Array.from(error['error']['error'][element]);
            validations.forEach(v => {
              mensajes.push(v);
            });
          });
        }      
      }
      return mensajes;
    }

    showErroresValidacion(error: HttpErrorResponse){
      let mensajes =this.getValidationMessages(error);
      
      
      if(mensajes.length >0){
        let html = '<p class="text-warning">Se encontraron los siguientes errores al validar la información:</p><ul class="list-group">';
        mensajes.forEach(element => {
          html+=`<li class="list-group-item">${element}</li>`
        });
        html +='</ul><p></p> <p class=" text-warning font-weight-bold">Por favor corrija la información y vuelva a intentarlo</p>';
        SweetAlert.html(false,'warning',html,'No se pudo completar el proceso', false, true);
        return true;
      }else return false;
    }
  
    guardar(){
      this.submitted = true;
      this.form.markAllAsTouched();
      if (this.form.valid) {
        SweetAlert.show(false, 'info', 'Procesando información', '', true); 
        this.formValid = true;
        if (this.accion === 'activar') {
          this.serviceActivar();
        } else if (this.accion === 'cambiar') {
          this.serviceChangePassword();
        }
  
      }else{
        this.formValid = false;
      }
    }
  
    salir(mensaje: string){
      this.matDialogRef.close(mensaje);
    }
  
    accionesGuardar(mensajeConfirmacion: string) {
      SweetAlert.show(false, 'success', '', mensajeConfirmacion, false, true);
      this.salir('OK');
    }



}
