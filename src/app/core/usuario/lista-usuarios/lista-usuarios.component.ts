import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Usuario } from '../../../models/usuario';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ServiceUsuario } from '../../../services/usuario.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { CreateUsuarioComponent } from '../create-usuario/create-usuario.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ViewKeysUsuarioComponent } from '../view-keys-usuario/view-keys-usuario.component';
import { ServiceTipoUsuario } from '../../../services/tipo-usuario.service';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.scss']
})
export class ListaUsuariosComponent implements OnInit {

  _ABREVIATURA= 'USERL';
  _ACCIONES = 'CRUDT';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  tipoAdmin = ConstantsService.typeUserAdmin;
  tipoVendor = ConstantsService.typeUserVendor;
  listaUsuarioAdmin: Usuario [] = [];
  listaUsuarioVendor: Usuario[] =[];
  nroUsuarios = 0;
  listaEstados = ConstantsService.listaEstadosUsuarios;
  isLoading = true;
  usuarioSeleccionado: Usuario = new Usuario();
  idNegocio = Number(ConstantsService.getNegocioKey());
  idTipoUsuarioNuevo = 0;
  //Información MatTable
  dataSourceVendor: MatTableDataSource<Usuario> = new MatTableDataSource<Usuario>(this.listaUsuarioAdmin);
  dataSourceAdmin: MatTableDataSource<Usuario> = new MatTableDataSource<Usuario>(this.listaUsuarioAdmin);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  // @ViewChild("paginatorVendor") paginatorVendor: MatPaginator =  ViewChild("paginatorVendor");
  // @ViewChild("paginatorAdmin") paginatorAdmin: MatPaginator =  ViewChild("paginatorAdmin");

  @ViewChild('paginatorVendor', { static: false }) set paginatorVendor(vendor: MatPaginator) {
    if(this.dataSourceVendor.data !== undefined && this.dataSourceVendor.data !== null && vendor !== undefined ){
      this.dataSourceVendor.paginator = vendor;
      vendor._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      vendor._intl.previousPageLabel = this.mensajes.itemAnterior;
      vendor._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  @ViewChild('paginatorAdmin', { static: false }) set paginatorAdmin(admin: MatPaginator) {
    if(this.dataSourceAdmin.data !== undefined && this.dataSourceAdmin.data !== null && admin !== undefined ){
      this.dataSourceAdmin.paginator = admin;
      admin._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      admin._intl.previousPageLabel = this.mensajes.itemAnterior;
      admin._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',
    'nameUsuario',
    'cedula',    
    'nombres',
    'apellidos',
    'estado',
    'genero',
    'telefono',
    'email',
    'direccion'
  ];

  constructor(private _serviceUsuario: ServiceUsuario, private matDialog: MatDialog, private mensajes: Mensajes,
    private _serviceTipoUsuario: ServiceTipoUsuario) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
   }

   ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getUsuariosVendor();  
      this.getUsuariosAdmin();
    }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false 
    && this._PERMISO.isAuthChangeType.state === false? 'mat-ocultar-columna' : '';
  }

  getUsuariosVendor() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de usuarios', true, false);
    }
    this._serviceUsuario.getUsersVendedor(this.idNegocio).subscribe( (result: Usuario[]) => {
      this.listaUsuarioVendor = result;
      this.asignarMatTableVendor(this.listaUsuarioVendor);
      // this.ngxDatatable.element.click();
      console.log(this.listaUsuarioVendor)
      this.nroUsuarios = this.listaUsuarioVendor.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTableVendor(usuarios: Usuario[]){
    this.dataSourceVendor.data = usuarios;
    // this.dataSourceVendor.paginator = this.paginatorVendor;
    // this.paginatorVendor._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginatorVendor._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginatorVendor._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  getUsuariosAdmin() {
    this.isLoading = true;
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de usuarios', true, false);
    }
    this._serviceUsuario.getUsersAdmin().subscribe(
      (result: Usuario[]) => {
      this.listaUsuarioAdmin = result;
      this.asignarMatTableAdmin(this.listaUsuarioAdmin);
      // this.ngxDatatable.element.click();
      this.nroUsuarios = this.listaUsuarioAdmin.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTableAdmin(usuarios: Usuario[]){
    this.dataSourceAdmin.data = usuarios;
    // this.dataSourceAdmin.paginator = this.paginatorAdmin;
    // this.paginatorAdmin._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginatorAdmin._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginatorAdmin._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string,tipoUsuario: string,usuario?: Usuario){
    return {
      accion,
      tipoUsuario,
      elemento: 'usuario',
      usuario: usuario ? usuario: null
    }
  }


  actionCreate(accion: string, tipoUsuario: string,usuario?: Usuario){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion, tipoUsuario);
    }else{
      datos = this.crearElementoModal(accion,tipoUsuario, usuario)
    }
    const dialogRef = this.matDialog.open(CreateUsuarioComponent, {
      width: '50%',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      if(data['respuesta'] ==='SAVE'){
      this.actionShowUserKeys(data);
      }
    });
  }

  actionShowUserKeys(datos: any){
    const dialogRef = this.matDialog.open(ViewKeysUsuarioComponent, {
      // width: '50%',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      if(data['tipo']=== this.tipoAdmin ){
        this.getUsuariosAdmin();
      }else{
        this.getUsuariosVendor();
      }
    });
  }

  cambiarEstado(usuario: Usuario, tipoUsuario:string){
    let mensaje =`Se cambiará el estado del usuario ${usuario.nameUsuario}`;
    // if(cuentaBancaria.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(usuario, tipoUsuario);
      }});
    // }
   }

   serviceCambiarEstado(usuario: Usuario, tipoUsuario:string){
       this._serviceUsuario.cambiarEstadoUsuario(usuario.idUsuario).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);        
        if(tipoUsuario === this.tipoAdmin ){
          this.getUsuariosAdmin();
        }else {
          this.getUsuariosVendor();
        }
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

  getEstado(estado: string){
    return this.listaEstados.find(x=> x.id=== estado).value;
  }

  actioncambiarTipo(usuario :Usuario, tipo: string){
    this.isLoading = true;
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de usuarios', true, false);
    }
    let tipoNuevo = this.getTipoUsuarioCambio(tipo);
    SweetAlert.confirm(`Se cambiarán los permisos del usuario ${usuario.nameUsuario}`,`El usuario ${usuario.nameUsuario} gozará de los privilegios de ${tipoNuevo} .
    Desea Continuar?`, 'warning', 'Si', 'No').then((result) => {
      if (result.value) {
        this.serviceCambiarTipoUsuario(this.generarEstructuraCambioTipo(usuario,tipoNuevo), usuario.idUsuario);
      }
    });
    
  }

  getTipoUsuarioCambio(tipo:string){
    let tipoNuevo= '';
    if(tipo === this.tipoAdmin){
      tipoNuevo ='VENDEDOR';
    }else{
      tipoNuevo ='ADMINISTRADOR';
    }
    return tipoNuevo;
  }

  generarEstructuraCambioTipo(usuario :Usuario, tipoNuevo:string){
    let data = {
      userId : usuario.idUsuario,
      tipoId: usuario.idTipo,
      tipoNameNuevo :tipoNuevo,
      negocioId: null
    }
    if(tipoNuevo ==='VENDEDOR'){
      data.negocioId = this.idNegocio;
    }

    return data;
  }

  serviceCambiarTipoUsuario(data: any, idUsuario: number){
    this._serviceUsuario.cambiarTipo(data, idUsuario).subscribe( result => {
      SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);        
      this.getUsuariosAdmin();
      this.getUsuariosVendor();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
    });
  }

  serviceGetIdTipoAdmin(){
    this._serviceTipoUsuario.getTipoAdmin().subscribe(result =>{
      this.idTipoUsuarioNuevo = result;  
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al obtener parametrización de tipos. Contacte a su administrador.');
    });
  }

  serviceGetIdTipoVendor(){
    this._serviceTipoUsuario.getTipoVendor().subscribe(result =>{
      this.idTipoUsuarioNuevo = result;  
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al obtener parametrización de tipos. Contacte a su administrador.');
    });
  }

  getIdTipoNuevo(tipoNombre: string){
    if(tipoNombre.length>0){
      let inicialTipo = tipoNombre[0];
      if(inicialTipo === ConstantsService.typeUserVendor){
        return this.serviceGetIdTipoVendor();
      }else{
        return this.serviceGetIdTipoAdmin();
      }
    }
  }
}
