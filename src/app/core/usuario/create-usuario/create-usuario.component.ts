import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Usuario } from '../../../models/usuario';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ServiceUsuario } from '../../../services/usuario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { ServiceTipoUsuario } from 'app/services/tipo-usuario.service';
import { ServicePersona } from '../../../services/persona.service';

@Component({
  selector: 'app-create-usuario',
  templateUrl: './create-usuario.component.html',
  styleUrls: ['./create-usuario.component.scss']
})
export class CreateUsuarioComponent implements OnInit {

  _ABREVIATURA= 'USERC';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  tipoUsuario: string;
  title: string;
  negocioKey = ConstantsService.getNegocioKey();
  usuarioSelected: Usuario = new Usuario();
  idTipoUsuario: number;
  elemento: string;

  // FormBuilder
  // form: FormGroup;
  submitted = false;
  formValid = true;
  isLoading = true;
  isValidPais = true;  

  listaGeneros = ConstantsService.listaGeneros;

  mainForm: FormGroup = new FormGroup({
    // pais_id: this.paisField,
    // usuario_credito: new FormControl('', [Validators.required, Validators.maxLength(45)]),
    cedula: new FormControl('', [Validators.required,Validators.maxLength(10)]),
    nombres: new FormControl('', [Validators.required,Validators.maxLength(100)]),
    apellidos: new FormControl('', [Validators.required,Validators.maxLength(100)]),
    genero:  new FormControl(''),
    telefono:  new FormControl('', [Validators.maxLength(15)]),
    direccion:  new FormControl('', Validators.maxLength(200)),
    email:  new FormControl('', [Validators.email, Validators.maxLength(45)]),
    ciudad:  new FormControl('', Validators.maxLength(150))
  });

  constructor(private _serviceUsuario: ServiceUsuario, private _serviceTipoUsuario: ServiceTipoUsuario,
     private _servicePersona: ServicePersona,private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log('PARAMETROS CREATE USUARIO',parametros)
      this.accion=parametros.accion;     
      this.elemento = parametros.elemento;
      this.tipoUsuario = parametros.tipoUsuario;
      this.usuarioSelected = parametros.usuario;      
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    // this.getPaises();
    this.initialize();    
  }
  
  initialize() {
    this.getTipoUsuario();
    if (this.accion === 'c') {
      this.title = 'Crear nuevo '+this.elemento;
    } else {
      this.title ='Editar ' +this.elemento;
      this.asignarUsuarioForm();
    }
    SweetAlert.close();  
  }


  public hasError = (controlName: string, errorName: string) => {
    return this.mainForm.controls[controlName].hasError(errorName);
  }

  asignarUsuarioCrear() {   
    let usuario: Usuario = this.mainForm.value as unknown as Usuario;
    if(this.tipoUsuario === ConstantsService.typeUserVendor){
      usuario.neg_id =parseInt(this.negocioKey);
    }
    if(this.usuarioSelected){
      usuario.idPersona = this.usuarioSelected.idPersona;
    }
    console.log(usuario);    
    return usuario;
    // let usuarioAux = this.mainForm.value;
    // usuarioAux.pais_id = usuarioAux.pais_id.id;
    // usuarioAux.neg_id = usuarioAux.pais_id.id;
    // let usuario = usuarioAux;
    // let usuario : Usuario = {
    //   // usuario_credito: this.mainForm.get('usuario_credito').value,
    //   nombres : this.mainForm.get('nombres').value,
    //   apellidos : this.mainForm.get('apellidos').value,
    //   telefono: this.mainForm.get('telefono').value,
    //   // pais_id: this.mainForm.get('pais_id').value['id'],
    //   // ci: this.mainForm.get('ci').value,
    //   ciudad: this.mainForm.get('ciudad').value,
    //   direccion: this.mainForm.get('direccion').value,
    //   email: this.mainForm.get('email').value,
    //   genero : this.mainForm.get('genero').value,
    //   // neg_id: Number(this.negocioKey)
    //   // usuario_registro:1
    // }
    // if(this.accion === 'c'){
    //   usuario.usuario_registro = 1;
    // }
    // console.log(usuario);
    
    // return usuario;
  }

  asignarUsuarioForm() {
    this.mainForm.patchValue(this.usuarioSelected);
    // this.mainForm.get('usuario_credito').setValue(this.usuarioSelected.usuario_credito);
    // this.mainForm.get('nombres').setValue(this.usuarioSelected.nombres);
    // this.mainForm.get('apellidos').setValue(this.usuarioSelected.apellidos);
    // this.mainForm.get('telefono').setValue(this.usuarioSelected.telefono);
    // this.mainForm.get('pais_id').setValue(this.cargarPaisSeleccionado(this.usuarioSelected.pais_id));
    // this.mainForm.get('ci').setValue(this.usuarioSelected.ci);
    // this.mainForm.get('ciudad').setValue(this.usuarioSelected.ciudad);
    // this.mainForm.get('direccion').setValue(this.usuarioSelected.direccion);
    // this.mainForm.get('email').setValue(this.usuarioSelected.email);
    // this.mainForm.get('genero').setValue(this.usuarioSelected.genero);
  }

  serviceCreateUsuarioAdmin(){
    this._serviceUsuario.saveUserAdmin(this.asignarUsuarioCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar(result);   
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar usuario');
    });
  }

  serviceCreateUsuarioVendor(){
    this._serviceUsuario.saveUserVendor(this.asignarUsuarioCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar(result);   
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', `${error['error']['error']}. ${error['error']['message']}`, 'Error al agregar usuario.');
    });
  }

  serviceGetIdTipoAdmin(){
    this._serviceTipoUsuario.getTipoAdmin().subscribe(result =>{
      this.idTipoUsuario = result;  
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al obtener parametrización de tipos. Contacte a su administrador.');
    });
  }

  serviceGetIdTipoVendor(){
    this._serviceTipoUsuario.getTipoVendor().subscribe(result =>{
      this.idTipoUsuario = result;  
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al obtener parametrización de tipos. Contacte a su administrador.');
    });
  }

  serviceUpdateUsuario(){
    this._servicePersona.updatePersona(this.asignarUsuarioCrear()).subscribe(result =>{
    this.accionesGuardar(result);
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar usuario');
    });
  }

  guardar(){
    this.submitted = true;
    this.mainForm.markAllAsTouched();
    if (this.mainForm.valid) {
      SweetAlert.show(false, 'info', 'Procesando información', '', true);
      this.formValid = true;
      if (this.accion === 'c') {
        this.guardarTipoUsuario();
      } else {
        this.serviceUpdateUsuario();
      }
      this.mainForm.reset();
    }else{
      this.formValid = false;
    }
  }

  guardarTipoUsuario(){
    if(this.tipoUsuario === ConstantsService.typeUserVendor){
      this.serviceCreateUsuarioVendor();
    }else{
      this.serviceCreateUsuarioAdmin();
    }
  }

  getTipoUsuario(){
    if(this.tipoUsuario === ConstantsService.typeUserVendor){
      this.serviceGetIdTipoVendor();
    }else{
      this.serviceGetIdTipoAdmin();
    }
  }

  salir(){
    this.matDialogRef.close({respuesta: 'OK'});
  }

  accionesGuardar(result: any) {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    if(result['data']){
      this.matDialogRef.close({respuesta: 'SAVE', user: result['data']['nameUser'], key: result['data']['keyUser'], tipo: this.tipoUsuario});
    }else{
      this.matDialogRef.close({respuesta: 'OK',tipo: this.tipoUsuario});
    }
  }
}
