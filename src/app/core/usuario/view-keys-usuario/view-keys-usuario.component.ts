import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';

@Component({
  selector: 'app-view-keys-usuario',
  templateUrl: './view-keys-usuario.component.html',
  styleUrls: ['./view-keys-usuario.component.scss']
})
export class ViewKeysUsuarioComponent implements OnInit {

  _ABREVIATURA= 'USERVK';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  tipoUsuario: string;
  user: string;
  key: string;
  credenciales: string;

  constructor(private matDialogRef: MatDialogRef<any>, @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.accion=parametros.accion;     
      this.tipoUsuario = parametros.tipo;    
      this.user = parametros.user;
      this.key = parametros.key;
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    this.generarTituloPagina();
    this.getCredenciales();
    SweetAlert.close();
    // this.getPaises();
    // this.initialize();    
  }

getCredenciales(){
  this.credenciales = 
  `
  Usuario:    ${this.user}
  Contraseña:  ${this.key}`;
}

generarTituloPagina(){
  let nombreTipo='';
  if(this.tipoUsuario === ConstantsService.typeUserVendor){
    nombreTipo = 'vendedor';
  }else {
    nombreTipo ='administrador';
  }
  this.title = `Usuario ${nombreTipo} creado exitosamente!`
}

  salir(){
    this.matDialogRef.close({respuesta: 'OK'});
  }

}
