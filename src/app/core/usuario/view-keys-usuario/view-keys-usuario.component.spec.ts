import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewKeysUsuarioComponent } from './view-keys-usuario.component';

describe('ViewKeysUsuarioComponent', () => {
  let component: ViewKeysUsuarioComponent;
  let fixture: ComponentFixture<ViewKeysUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewKeysUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewKeysUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
