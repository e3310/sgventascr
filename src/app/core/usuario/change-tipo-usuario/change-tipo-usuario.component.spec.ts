import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeTipoUsuarioComponent } from './change-tipo-usuario.component';

describe('ChangeTipoUsuarioComponent', () => {
  let component: ChangeTipoUsuarioComponent;
  let fixture: ComponentFixture<ChangeTipoUsuarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeTipoUsuarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeTipoUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
