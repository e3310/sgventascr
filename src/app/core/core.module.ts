import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, DatePipe } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CoreRoutingModule } from './core-routing.module';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule} from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs'; 
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatDatepickerModule } from '@angular/material/datepicker'; 
import { MatNativeDateModule, MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import {MatDividerModule} from '@angular/material/divider'; 
//Fechas
import { MAT_DATE_LOCALE } from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox'; 
// import { FileUploadModule } from 'ng2-file-upload';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { WelcomeComponent } from './welcome/welcome.component';
import { ListaNegociosComponent } from './negocio/lista-negocios/lista-negocios.component';
import { CreateNegocioComponent } from './negocio/create-negocio/create-negocio.component';
import { ListaPaisesComponent } from './pais/lista-paises/lista-paises.component';
import { CreatePaisComponent } from './pais/create-pais/create-pais.component';
import { ListaClientesComponent } from './cliente/lista-clientes/lista-clientes.component';
import { CreateClienteComponent } from './cliente/create-cliente/create-cliente.component';
import { SeleccionarNegocioComponent } from './negocio/seleccionar-negocio/seleccionar-negocio.component';
import { CargaMasivaComponent } from './cliente/carga-masiva/carga-masiva.component';
import { ListaPaquetesComponent } from './producto/lista-paquetes/lista-paquetes.component';
import { CreatePaqueteComponent } from './producto/create-paquete/create-paquete.component';
import { ParametrizarProductoComponent } from './producto/parametrizar-producto/parametrizar-producto.component';
import { ListaTipoFormaPagoComponent } from './tipo-forma-pago/lista-tipo-forma-pago/lista-tipo-forma-pago.component';
import { CreateTipoFormaPagoComponent } from './tipo-forma-pago/create-tipo-forma-pago/create-tipo-forma-pago.component';
import { CreateFormaPagoComponent } from './forma-pago/create-forma-pago/create-forma-pago.component';
import { ListaFormaPagoComponent } from './forma-pago/lista-forma-pago/lista-forma-pago.component';
import { ListaUsuariosComponent } from './usuario/lista-usuarios/lista-usuarios.component';
import { CreateUsuarioComponent } from './usuario/create-usuario/create-usuario.component';
import { ViewKeysUsuarioComponent } from './usuario/view-keys-usuario/view-keys-usuario.component';
import { GetKeysUsuarioComponent } from './usuario/get-keys-usuario/get-keys-usuario.component';
import { ChangeTipoUsuarioComponent } from './usuario/change-tipo-usuario/change-tipo-usuario.component';
import { ListaVendedoresComponent } from './vendedor/lista-vendedores/lista-vendedores.component';
import { ListaComprasComponent } from './compra/lista-compras/lista-compras.component';
import { CreateCompraComponent } from './compra/create-compra/create-compra.component';
import { ListaVentasComponent } from './venta/lista-ventas/lista-ventas.component';
import { CreateVentaComponent } from './venta/create-venta/create-venta.component';
import { ListaKardexComponent } from './kardex/lista-kardex/lista-kardex.component';
import { ErrorAccessComponent } from './error-access/error-access.component';
import { ChangePasswordComponent } from './usuario/change-password/change-password.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatchHeightModule,
        CoreRoutingModule,
        NgxDatatableModule,
        MatDialogModule,
        MatTooltipModule,
        MatAutocompleteModule,
        MatInputModule,
        MatSelectModule,
        MatRadioModule,
        MatListModule,
        MatIconModule,
        MatTableModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatGridListModule,
        MatTabsModule,
        ClipboardModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        NgbModule,
        MatDividerModule
        // FileUploadModule
        // BrowserModule
    ],
    declarations: [
        WelcomeComponent,
        ListaNegociosComponent,
        CreateNegocioComponent,
        ListaPaisesComponent,
        CreatePaisComponent,
        ListaClientesComponent,
        CreateClienteComponent,
        SeleccionarNegocioComponent,
        CargaMasivaComponent,
        ListaPaquetesComponent,
        CreatePaqueteComponent,
        ParametrizarProductoComponent,
        ListaTipoFormaPagoComponent,
        CreateTipoFormaPagoComponent,
        CreateFormaPagoComponent,
        ListaFormaPagoComponent,
        ListaUsuariosComponent,
        CreateUsuarioComponent,
        ViewKeysUsuarioComponent,
        GetKeysUsuarioComponent,
        ChangeTipoUsuarioComponent,
        ListaVendedoresComponent,
        ListaComprasComponent,
        CreateCompraComponent,
        ListaVentasComponent,
        CreateVentaComponent,
        ListaKardexComponent,
        ErrorAccessComponent,
        ChangePasswordComponent
    ], 
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA 
    ],
    exports: [WelcomeComponent],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-EC' },
        NgbModule,
        DatePipe
        // { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        // { provide: MAT_DATE_FORMATS, useValue: MOMENT_FORMATS}
    ]
})
export class CoreModule { }