import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SweetAlert } from 'app/utils/use-sweet-alert';
import { Negocio } from '../../../models/negocio';
import { ServiceNegocio } from '../../../services/negocio.service';
import {
  ColumnMode,
  DatatableComponent,
  SelectionType
} from '@swimlane/ngx-datatable';
import {MatDialog} from '@angular/material/dialog';
import { CreateNegocioComponent } from '../create-negocio/create-negocio.component';
import { ConstantsService } from '../../../services/constants.service';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Mensajes } from '../../../utils/mensajes';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-negocios',
  templateUrl: './lista-negocios.component.html',
  styleUrls: ['./lista-negocios.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListaNegociosComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'NEGL';
  _ACCIONES = 'CRUD';  
  _PERMISO = new Permiso();

  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaNegocios: Negocio [] = [];
  nroNegocios = 0;
  listaEstados = ['ACTIVO','INACTIVO']
  isLoading = true;
  negocioSeleccionado: Negocio = new Negocio();
  @ViewChild('ngxDatatableNegocios') ngxDatatable: DatatableComponent;


  //Información MatTable
  dataSource: MatTableDataSource<Negocio> = new MatTableDataSource<Negocio>(this.listaNegocios);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(negocio: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && negocio !== undefined ){
      this.dataSource.paginator = negocio;
      negocio._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      negocio._intl.previousPageLabel = this.mensajes.itemAnterior;
      negocio._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }

  displayedColumns: string[] = [
    'acciones',
    'nombre',
    'estado',
    'fechaRegistro'
  ];
  constructor(private _serviceNegocio: ServiceNegocio, private matDialog: MatDialog, private router: Router, private mensajes: Mensajes) {
    ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
  }

   ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getNegocios();  
    }
   }

   ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state === false ? 'mat-ocultar-columna' : '';
  }

   getNegocios() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de negocios', true, false);
    }
    this._serviceNegocio.getNegocios().subscribe( (result: Negocio[]) => {
      this.listaNegocios = result;
      this.asignarMatTable(this.listaNegocios);
      // this.ngxDatatable.element.click();
      console.log(this.listaNegocios)
      this.nroNegocios = this.listaNegocios.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(negocios: Negocio[]){
    this.dataSource.data = negocios;
    this.dataSource.paginator = this.paginator;
  }

  crearElementoModal(accion: string,negocio?: Negocio){
    return {
      accion,
      negocio: negocio ? negocio: null
    }
  }


  actionCreate(accion: string,negocio?: Negocio){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, negocio)
    }
    const dialogRef = this.matDialog.open(CreateNegocioComponent, {
      // width: '90%',
      // height: '90%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getNegocios();
    });
  }

  cambiarEstado(negocio: Negocio){
    let mensaje =`Se cambiará el estado del negocio ${negocio.nombre}`;
    // if(negocio.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(negocio);
      }});
    // }
   }

   serviceCambiarEstado(negocio: Negocio){
       this._serviceNegocio.cambiarEstadoNegocio(negocio.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getNegocios();
      }, (error: HttpErrorResponse) => {
        console.log(error);
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
      });
   }

   accionSelectNegocio(){
    console.log(this.nroNegocios);
    let negocioSeleccionado = this.listaNegocios[0];
    if(negocioSeleccionado){
      if(negocioSeleccionado.estado === 'A'){
        this.accionChangeValues(negocioSeleccionado);         
      }else{
        SweetAlert.show(false,'error', 'No puede seleccionar este negocio', 'Negocio Inactivo', false, true);
      }
    }else{
      SweetAlert.show(false,'error', 'No existen Negocios', 'Error', false, true);
    }
  }

  accionChangeValues(negocioSeleccionado: Negocio){
    ConstantsService.idNegocio = negocioSeleccionado.id;     
    ConstantsService.nombreNegocio = negocioSeleccionado.nombre; 
    ConstantsService.saveSessionNegocio();   

    SweetAlert.show(false, 'success', `Toda la información en el sistema se visualizará en base a la información del negocio ${negocioSeleccionado.nombre.toUpperCase()}.
    La página se recargará en 3 segundos`,
    `Ha seleccionado ${negocioSeleccionado.nombre.toUpperCase()}`,false, true);
    setTimeout(() => {
      this.refreshPage();
    }, 3000);      
  }
  

  // actionActualizarLista(){
  //   let a = this.matDialog.afterAllClosed;
  //   console.log(a);
    
  // }

  // async ngOnInit(): Promise<void> {    
  //   await this.getNegocios()
  //   this.listaNegocios = [...this.listaNegocios]
  // }

  // getVista(): Promise<Negocio[]>{
  //   console.log(2)
  //   return this._serviceNegocio.getNegocios();
  // }


  // async getNegocios(): Promise<void> {
  //   console.log(1)
  //   console.log(this.isLoading);
  //   try{
  //   if (this.isLoading) {
  //     SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de negocios', true, false);
  //   }

  //   const a = await this.getVista();   
    
  //   this.listaNegocios = a;
  //   this.ngxDatatable.element.click();

  //   this.isLoading = false;
  //   SweetAlert.close();    
  //   /* this._serviceNegocio.getNegocios().then( (result: Negocio[]) => {
  //     this.listaNegocios = result;
  //     SweetAlert.close();      
  //     this.isLoading = false;
  //   }).catch((error: HttpErrorResponse) => {
  //   // }, (error: HttpErrorResponse) => {
  //     console.log(error);
  //     SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
  //   }); */
  //   console.log(a);
  // }catch (error){
  //   SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
  // }
  //   return Promise.resolve();
  // }
  refreshPage(){
    this.router.routeReuseStrategy.shouldReuseRoute = ()=> false;
    this.router.onSameUrlNavigation ='reload';
    this.router.navigate(['/core/negocio/lista']);
    window.location.reload(); 
  }


}
