import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { Negocio } from '../../../models/negocio';
import { ServiceNegocio } from '../../../services/negocio.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConstantsService } from '../../../services/constants.service';

@Component({
  selector: 'app-create-negocio',
  templateUrl: './create-negocio.component.html',
  styleUrls: ['./create-negocio.component.scss']
})
export class CreateNegocioComponent implements OnInit {

  _ABREVIATURA= 'NEGC'
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioSelected: Negocio = new Negocio();

  // FormBuilder
  form: FormGroup;
  submitted = false;
  formValid = true;

  constructor(private fb: FormBuilder, private _serviceNegocio: ServiceNegocio, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      console.log(parametros)
      this.accion=parametros.accion;
      this.negocioSelected = parametros.negocio;
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);
    this.initValidators();
    this.initialize();
  }

  initialize() {
    if (this.accion === 'c') {
      this.title = 'Crear nuevo negocio';
      this.form.get('nombre').setValue('');      
    } else {
      this.title ='Editar Negocio';
      this.form.get('nombre').setValue(this.negocioSelected.nombre);
    }
    SweetAlert.close();
  }

  initValidators() {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.maxLength(100)]]
     });
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarNegocioCrear() {
    let negocio : Negocio = {
      nombre : this.form.get('nombre').value,
      usuario_registro: Number(ConstantsService.getUserKey())
    }
    return negocio;
  }

  asignarNegocioActualizar() {
    let negocio : Negocio = {
      nombre : this.form.get('nombre').value
    }
    return negocio;
  }

  serviceCreatenegocio(){
    this._serviceNegocio.saveNegocio(this.asignarNegocioCrear()).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar negocio');
    });
  }

  serviceUpdatenegocio(){
    this._serviceNegocio.updateNegocio(this.asignarNegocioActualizar(), this.negocioSelected.id).subscribe(result =>{
    this.accionesGuardar();
    }, (error ) => {
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al editar negocio');
    });
  }

  guardar(){
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        console.log('creacion');
        this.serviceCreatenegocio();
        // this.verificarCreacionChofer();
      } else {
        console.log('update');
        this.serviceUpdatenegocio();
      }
    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.salir();
  }

}
