import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarNegocioComponent } from './seleccionar-negocio.component';

describe('SeleccionarNegocioComponent', () => {
  let component: SeleccionarNegocioComponent;
  let fixture: ComponentFixture<SeleccionarNegocioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionarNegocioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
