import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { Negocio } from '../../../models/negocio';
import { ServiceNegocio } from '../../../services/negocio.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';


@Component({
  selector: 'app-seleccionar-negocio',
  templateUrl: './seleccionar-negocio.component.html',
  styleUrls: ['./seleccionar-negocio.component.scss']
})
export class SeleccionarNegocioComponent implements OnInit {
  
  _ABREVIATURA= 'NEGSL'
  listaNegocios: Negocio [] = [];
  listaEstados = ['ACTIVO','INACTIVO'];
  isLoading = true;
  listaVacia = false;

  constructor(private _serviceNegocio: ServiceNegocio, private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any, private router: Router) { }

  ngOnInit(){
    // console.log(ConstantsService.getNegocioKey());    
    // if(ConstantsService.getNegocioKey() === null){
      this.getNegocios();
    // }
  }

  getNegocios() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de negocios', true, false);
    }
    this._serviceNegocio.getNegocios().subscribe( (result: Negocio[]) => {
      this.listaNegocios = result;
      console.log(this.listaNegocios)
      SweetAlert.close();      
      if(this.listaNegocios.length <= 0){
        this.listaVacia = true;
      }
      // this.activemodal = this.modalService.open(this.templateRef, { scrollable: true, centered: true, backdrop: 'static' });
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      // this.listaVacia = true;
      if(error['status']===500){
        this.listaVacia = false;
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Servicio no disponible' , 'Contacte con su administrador', false, false);
      }else{
        this.listaVacia = false;
        SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información', false, false);
      }
      
    });
  }

accionSelectNegocio(negocioSeleccionado: Negocio){
 if(this.accionVerificarEstadoNegocio(negocioSeleccionado)=== true){
  this.salir('OK');
 }
}

accionVerificarEstadoNegocio(negocioSeleccionado: Negocio){
  if(negocioSeleccionado.estado === 'A'){
    this.accionChangeValues(negocioSeleccionado); 
    return true;        
  }else{
    SweetAlert.show(false,'error', 'No puede seleccionar este negocio', 'Negocio Inactivo', false, true);
    return false;
  }
}

  accionChangeValues(negocioSeleccionado: Negocio){
    ConstantsService.idNegocio = negocioSeleccionado.id;     
    ConstantsService.nombreNegocio = negocioSeleccionado.nombre; 
    ConstantsService.saveSessionNegocio();   
    SweetAlert.show(false, 'success', `Toda la información en el sistema se visualizará en base a la información del negocio ${negocioSeleccionado.nombre.toUpperCase()}.
    La página se recargará en 3 segundos.`,
    `Ha seleccionado ${negocioSeleccionado.nombre.toUpperCase()}`,false, true)
  }

  salir(mensaje: string){
    this.matDialogRef.close(mensaje);
  }
  
  navigateListaNegocios(){
    this.router.navigate(['/core/negocio/lista']);
    this.salir('EMPTY');
  }
}