import { Component, OnInit, ViewChild } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Compra } from '../../../models/compra';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ServiceCompra } from '../../../services/compra.service';
import { MatDialog } from '@angular/material/dialog';
import { Mensajes } from '../../../utils/mensajes';
import { SweetAlert } from '../../../utils/use-sweet-alert';
import { HttpErrorResponse } from '@angular/common/http';
import { CreateCompraComponent } from '../create-compra/create-compra.component';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { AppDateAdapter, MY_FORMATS } from '../../directives/format-datepicker';
import * as _moment from 'moment';
import {defaultFormat as _rollupMoment} from 'moment';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { ExportXLSX } from '../../../utils/exportXLSX';
import { PdfMake } from '../../../utils/pdfMake';
import { Permiso } from '../../../models/permisos';

@Component({
  selector: 'app-lista-compras',
  templateUrl: './lista-compras.component.html',
  styleUrls: ['./lista-compras.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: AppDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class ListaComprasComponent implements OnInit {

  //Seguridad por usuarios
  _ABREVIATURA= 'COML';
  _ACCIONES = 'CRDS';  
  _PERMISO = new Permiso();

  diasValidos = 3;
  negocioKeySesion =ConstantsService.getNegocioKey();
  negocioValueSesion = ConstantsService.getNegocioValue();
  listaCompras: Compra [] = [];
  listaTempCompras: Compra [] = [];

  nroCompras = 0;
  listaEstados = ConstantsService.listaEstados
  listaTiposCuenta = ConstantsService.listaTiposCuenta;
  isLoading = true;
  compraSeleccionada: Compra = new Compra();
  idNegocio = Number(ConstantsService.getNegocioKey());
  //Información MatTable
  dataSource: MatTableDataSource<Compra> = new MatTableDataSource<Compra>(this.listaCompras);
  pageSize = ConstantsService.pageSize;
  pageSizeOptions = ConstantsService.pageSizeOptions;
  @ViewChild('paginator', { static: false }) set paginator(compra: MatPaginator) {
    if(this.dataSource.data !== undefined && this.dataSource.data !== null && compra !== undefined ){
      this.dataSource.paginator = compra;
      compra._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
      compra._intl.previousPageLabel = this.mensajes.itemAnterior;
      compra._intl.nextPageLabel = this.mensajes.itemSiguiente;
    }
  }
  displayedColumns: string[] = [
    'acciones',
    'fecha_registro',
    'estado',
    'cantidad',
    'costo',
    'total'
  ];

  // FormBuilder de búsqueda
  searchForm: FormGroup;
  submitted = false;
  formValid = true;
  isBusquedaActiva = false;
  fechaInicioField = new FormControl(new Date(),Validators.required);
  fechaFinField = new FormControl(new Date(),Validators.required);
  //Checbox descarga
  isVisualizar = false;
  isDescargar = true;
  listaComprasSearch: Compra [] = [];

  currentDay : Date;
  firstDayMonth : Date;
  lastDayMonth : Date;
  minDate : Date;

  constructor(private _serviceCompra: ServiceCompra, private matDialog: MatDialog, private mensajes: Mensajes,
    private fb: FormBuilder) {
      ConstantsService.verifyPermisos(this._PERMISO,this._ABREVIATURA, this._ACCIONES);
      this.currentDay = new Date();
      this.firstDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth(), 1);
      this.lastDayMonth = new Date(this.currentDay.getFullYear(), this.currentDay.getMonth() + 1, 0);
      this.minDate = new Date(2021,0,1);
   }

  ngOnInit():void{
    if(this._PERMISO.isAuthorized.state === true){
      this.getCompras();    
    }
  }

  ocultarColumnaAcciones() {
    return this._PERMISO.isAuthUpdate.state === false && this._PERMISO.isAuthDesactivate.state == false ? 'mat-ocultar-columna' : '';
  }

   getCompras() {
    if (this.isLoading) {
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando lista de compras', true, false);
    }
    this._serviceCompra.getCompras(this.idNegocio).subscribe( (result: Compra[]) => {
      this.listaCompras = result;
      this.listaTempCompras = result;
      this.asignarMatTable(this.listaCompras);
      // this.ngxDatatable.element.click();
      console.log(this.listaCompras)
      this.nroCompras = this.listaCompras.length;
      SweetAlert.close();            
      this.isLoading = false;
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

  asignarMatTable(compras: Compra[]){
    this.dataSource.data = compras;
    // this.dataSource.paginator = this.paginator;
    // this.paginator._intl.itemsPerPageLabel = this.mensajes.itemsPorPagina;
    // this.paginator._intl.previousPageLabel = this.mensajes.itemAnterior;
    // this.paginator._intl.nextPageLabel = this.mensajes.itemSiguiente;
  }

  crearElementoModal(accion: string,compra?: Compra){
    return {
      accion,
      compra: compra ? compra: null
    }
  }


  actionCreate(accion: string,compra?: Compra){
    let datos = null;
    if (accion === 'c') {
      datos = this.crearElementoModal(accion);
    }else{
      datos = this.crearElementoModal(accion, compra)
    }
    console.log(accion, datos);
    
    const dialogRef = this.matDialog.open(CreateCompraComponent, {
      width: '20%',
      // height: '70%',      
      disableClose: true,
      data: datos
    });
    dialogRef.afterClosed().subscribe(data => {
      this.getCompras();
    });
  }

  cambiarEstado(compra: Compra){
    let mensaje =`Se cambiará el estado del compra C-${compra.id}`;
    // if(compra.estado === this.listaEstados[0]){
      SweetAlert.confirm(mensaje,
      'Desea Continuar?', 'warning', 'Si', 'Cancelar').then((result) => {
        if (result.value) {
          this.serviceCambiarEstado(compra);
      }});
    // }
   }

   serviceCambiarEstado(compra: Compra){
       this._serviceCompra.cambiarEstadoCompra(compra.id).subscribe( result => {
        SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
        this.getCompras();
      }, (error: HttpErrorResponse) => {
        if(error['status']===403){
          SweetAlert.show(false, 'error', `No puede anular compras registradas hace más de ${this.diasValidos} dias.` ,
           'Imposible cambiar estado');
        }else{
          console.log(error);
          SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cambiar estado');
        }
      });
   }

   // BUSCAR COMPRAS
   initSearchForm() {
    this.searchForm = this.fb.group({
      inicio: this.fechaInicioField,
      fin: this.fechaFinField,
      visualizar: new FormControl(false),
      descargar: new FormControl(true)
     });
  }

  actionViewFormSearch(){
    this.initSearchForm();  
    this.isBusquedaActiva = true;
  }

  actionHideFormSearch(){
    SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);
    this.isBusquedaActiva = false;    
    this.listaCompras = [...this.listaTempCompras];
    this.dataSource.data = this.listaCompras;
    this.initSearchForm();
    SweetAlert.close();
  }

  actionSearch(tipo: string){
    this.submitted = true;
    this.searchForm.markAllAsTouched();
    this.listaComprasSearch = [];
    if(this.searchForm.valid){
      this.formValid = true;
      this.isLoading = true;
      SweetAlert.show(false, 'info', 'Espere un momento', 'Cargando resultados de búsqueda', true, false);     
      this.serviceSearch(tipo);
      this.isLoading = false;
    }else{
      console.log(this.searchForm);      
      this.formValid = false;
    }
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.searchForm.controls[controlName].hasError(errorName);
  }

  actionChange(checked: boolean, accion: string){
    if(accion === 'v' && checked){
      this.searchForm.controls['descargar'].setValue(false);
      this.isVisualizar = true;
      this.isDescargar = false;
    }else if(accion === 'd' && checked){
      this.searchForm.controls['visualizar'].setValue(false);
      this.isVisualizar = false;
      this.isDescargar = true;
    }   
  }

  actionSearchVisualizar(result: Compra[]){
    this.listaCompras =[...result];
    this.dataSource.data = result;
  }

  actionSearchDescargar(result: Compra[], tipo: string){
    let reporte = [];
      result.forEach((e: Compra) => {
        let item = [ e.fecha_registro, e.cantidad, e.costo, e.total];
          reporte.push(item);
      });
      if  (reporte.length > 0){
        let encabezado = ['FECHA REGISTRO', 'CANTIDAD', 'COSTO UNITARIO', 'TOTAL COMPRA'];
        switch(tipo){
          case 'p':
            this.descargarReportePdf(encabezado,reporte);
            break;
          case 'e':
            this.descargarReporteExcel(encabezado,reporte);
            break;
        }
        SweetAlert.close();
      }else{
        SweetAlert.show(false, 'warning', 'No se encontraron resultados para la búsqueda', 'No se ha generado el archivo', false, true); 
      }      
  }

  descargarReporteExcel(encabezado: any,lista: any[]){    
    ExportXLSX.downloadXLSX(encabezado, lista, 'Compras_por_fecha');
    // SweetAlert.close();
  }

  descargarReportePdf(encabezado: any,lista: any[]) {
    var pdfGenerator =
    PdfMake.createPdf(encabezado, lista, 'Compras por fecha', 'Administrador', 'landscape');
    PdfMake.downloadPdf(pdfGenerator, 'Compras_por_fecha');
  }

  serviceSearch(tipo: string){
    this._serviceCompra.searchCompras(this.idNegocio,this.searchForm.get('inicio').value,this.searchForm.get('fin').value).subscribe( (result: Compra[]) => {
      if(this.searchForm.controls['visualizar'].value === true){
      this.actionSearchVisualizar(result);
      }else{
        this.actionSearchDescargar(result, tipo);
      }
      // SweetAlert.close();
    }, (error: HttpErrorResponse) => {
      console.log(error);
      SweetAlert.show(false, 'error', error['message'] != null  ? error['message'] : 'Error Inesperado' , 'Imposible cargar información');
    });
  }

}
