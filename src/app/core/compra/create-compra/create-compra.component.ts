import { Component, OnInit, Inject } from '@angular/core';
import { ConstantsService } from '../../../services/constants.service';
import { Compra } from '../../../models/compra';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ServiceCompra } from '../../../services/compra.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SweetAlert } from '../../../utils/use-sweet-alert';

@Component({
  selector: 'app-create-compra',
  templateUrl: './create-compra.component.html',
  styleUrls: ['./create-compra.component.scss']
})
export class CreateCompraComponent implements OnInit {

  _ABREVIATURA= 'COMC';
  usuarioLogueado = ConstantsService.decrypt(sessionStorage.getItem('userKey'));
  accion:string;
  title: string;
  negocioKey = ConstantsService.getNegocioKey();
  compraSelected: Compra = new Compra();
  stringCuentaBancaria = ConstantsService.stringCuentaBancaria;

  // FormBuilder
  // form: FormGroup;
  submitted = false;
  formValid = true;

  isLoading = true;
  isCuentaBancaria = false;

  listaCompras : Compra[] = [];

  form: FormGroup = new FormGroup({
    cantidad: new FormControl('', [Validators.required, Validators.min(1)]),
    costo: new FormControl(0, [Validators.required,Validators.min(0.1)]),
    total: new FormControl('', [Validators.required, Validators.min(0.1)])
  });
  
  constructor(private _serviceCompra: ServiceCompra, 
     private matDialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public parametros: any) { 
      // this.initValidators();
      console.log(parametros)
      this.accion=parametros.accion;     
      this.compraSelected = parametros.compra;
      
    }

  ngOnInit(): void {
    SweetAlert.show(false, 'info', 'Cargando información', '', true);    
    this.initialize();    
  }

  initialize() {    
    if (this.accion === 'c') {
      this.title = 'Registrar compra';
    } else { // accion s => show
      // this.isShowAction = true;
      this.compraSelected = this.parametros.kardex;   
      this.title = `Compra de origen C-${this.compraSelected.id}`;
      this.asignarCompraForm();
      this.form.disable();
    }
    SweetAlert.close();  
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }

  asignarCompraCrear() {   
    let compraCrear: Compra = this.form.value as unknown as Compra;
    // let auxSaldo = cuentaBancaria.saldo? cuentaBancaria.saldo: 0;
    // cuentaBancaria.saldo = auxSaldo;
    compraCrear.neg_id =parseInt(this.negocioKey);
    compraCrear.usuario_registro = parseInt(ConstantsService.getUserKey());
    // console.log(compraCrear);    
    return compraCrear;
  }

  serviceCreateCompra(){
    this._serviceCompra.saveCompra(this.asignarCompraCrear()).subscribe(result =>{
      console.log(result);
      this.accionesGuardar();  
    
    }, error => {
      console.log(error);
      SweetAlert.show(false, 'error', error['error']['error'], 'Error al agregar compra');
    });
  }
  guardar(){
    console.log('form', this.form);
    SweetAlert.show(false, 'info', 'Procesando información', '', true);   
    this.submitted = true;
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.formValid = true;
      if (this.accion === 'c') {
        this.serviceCreateCompra();
      } 

    }else{
      this.formValid = false;
    }
  }

  salir(){
    this.matDialogRef.close('OK');
  }

  accionesGuardar() {
    SweetAlert.show(false, 'success', 'Cambios Guardados', '', false, false,1000);
    this.matDialogRef.close('SAVE');
  }

  getCostoUnitario(){    
    let unitario=0;
    if(this.form.controls['total'].value !== '' && this.form.controls['cantidad'].value !== ''){
      let costoTotal = parseFloat(this.form.controls['total'].value);
      let stock = parseInt(this.form.controls['cantidad'].value );
      unitario = Math.round((costoTotal/stock)*10000)/10000;
    }
    this.form.controls['costo'].setValue(unitario);
  }

  asignarCompraForm() {
    this.form.patchValue(this.compraSelected);
  }

}
